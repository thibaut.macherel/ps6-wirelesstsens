/**
 * @file    hal_clock.h
 * @author  Jan Grossrieder
 * @date    November, 2014
 * @brief   Handles the initialization and start of the CPU- and RTC-clock
 */

#ifndef HAL_CLOCK_H_INCLUDED
#define HAL_CLOCK_H_INCLUDED

/* includes that must be known for this header -----------------------------------------------*/
#include <stdint.h>
#include <stdbool.h>
#include "nrf.h"

/*---------------------------------------------------------------------------------------------+
|  global constants, macros and enums                                                          |
+---------------------------------------------------------------------------------------------*/
/**
 * Clock frequency types
 */
typedef enum
{
   CLOCK_HF_FREQUENCY_16MHZ = CLOCK_XTALFREQ_XTALFREQ_16MHz,   /**< Crystal clock 16MHz */
   CLOCK_HF_FREQUENCY_32MHZ = CLOCK_XTALFREQ_XTALFREQ_32MHz    /**< Crystal clock 32MHz */
} e_clockFreq_t;

/**
 * Configuration struct for the clock
 */
typedef struct
{
   bool isHFClockEnabled;        /**< true if HF-clock (sytsem clock) enabled, false if not */
   e_clockFreq_t e_clockHFFreq;  /**< crystal clock frequency */
   bool isLFClockEnabled;        /**< true if LF-clock (RTC) enabled, false if not */
} s_clockConfig_t;

/*---------------------------------------------------------------------------------------------+
|  function prototypes                                                                         |
+---------------------------------------------------------------------------------------------*/
/**
 * Initialize the system and RTC clock.
 *
 * @param p_config_p the configuration
 */
void halClockInit(const s_clockConfig_t *const p_config_p);

#endif
