/**
 * @file    hal_rtc.c
 * @author  Thibaut Macherel
 * @date    Mars, 2015
 * @brief   see hal_wdt.h
 */

/* headers for this module -------------------------------------------------------------------*/
#include "hal_wdt.h"
#include "nrf.h"

/* standard headers (ANSI- and Posix standards) ----------------------------------------------*/
#include <stdint.h>
#include <stdbool.h>
#include <stdio.h>

/*---------------------------------------------------------------------------------------------+
|  module global variables                                                                     |
+---------------------------------------------------------------------------------------------*/
/**
 * The registered callback function
 */
//static t_wdtIntCb *wdtCallbackFunc_s = NULL;


/*---------------------------------------------------------------------------------------------+
|  functions                                                                                   |
+---------------------------------------------------------------------------------------------*/
void halWDTInit(const s_wdtConfig_t *const p_config_p)
{
   /* power on WDT peripherie */
   NRF_WDT->POWER = (WDT_POWER_POWER_Enabled << WDT_POWER_POWER_Pos);
	
	 NVIC_SetPriority(WDT_IRQn, 2U);
   NVIC_EnableIRQ(WDT_IRQn);
	
	 /* config the wdt */
	 if(p_config_p->isRunningWhileCpuSleep == true) 
	 {
		 NRF_WDT->CONFIG |= (WDT_CONFIG_HALT_Pause << WDT_CONFIG_HALT_Pos);
	 }
	 if(p_config_p->isRunningWhileHaltedDebug == true)
	 {
		 NRF_WDT->CONFIG |= ( WDT_CONFIG_SLEEP_Run << WDT_CONFIG_SLEEP_Pos);
	 }	 
	 if(p_config_p->isIntEnabledSetRegister == true)
	 {
		 NRF_WDT->INTENSET |= (WDT_INTENSET_TIMEOUT_Enabled << WDT_INTENSET_TIMEOUT_Pos);
	 }
	 
	 NRF_WDT->CRV = p_config_p->ul_CRV;
	 
	 NRF_WDT->TASKS_START = 1UL;
	 
	 NRF_WDT->INTENCLR = WDT_INTENCLR_TIMEOUT_Disabled << WDT_INTENCLR_TIMEOUT_Pos;
	 
}

void WDT_IRQHandler(void)
{
   
   NRF_WDT->INTENCLR = WDT_INTENCLR_TIMEOUT_Disabled << WDT_INTENCLR_TIMEOUT_Pos;
}

