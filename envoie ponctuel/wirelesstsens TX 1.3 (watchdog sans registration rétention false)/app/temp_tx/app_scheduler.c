/**
 * @file    app_scheduler.c
 * @author  Jan Grossrieder
 * @date    November, 2014
 * @brief   see @ref app_schedTx_desc
 */

/* headers for this module -------------------------------------------------------------------*/
#include "app_scheduler.h"
#include "app_radio_tx.h"
#include "hal_wdt.h"
#include "hal_temp.h"
#include "hal_power.h"

/* standard headers --------------------------------------------------------------------------*/
#include <stdint.h>
#include <stdbool.h>
#include <stdio.h>

/*---------------------------------------------------------------------------------------------+
|  local constants and macros                                                                  |
+---------------------------------------------------------------------------------------------*/
#define WDT_INTERVAL_ms 100UL /**< temps de l'interval du watchdog en ms */
//#define WDT_crv ((WDT_INTERVAL_ms*32768UL)/1000UL)-1UL
#define WDT_crv 3


/*---------------------------------------------------------------------------------------------+
|  module global variables                                                                     |
+---------------------------------------------------------------------------------------------*/
/** WDT configuration */
static const s_wdtConfig_t s_wdtConf_s = {true, true, true, WDT_crv};


/*---------------------------------------------------------------------------------------------+
|  functions                                                                                   |
+---------------------------------------------------------------------------------------------*/
void appSchedulerInit(void)
{
//   uint16_t us_dummyCtr;
   
	  
   
//   /* send registration frame on startup */
//   appRadioTxSendReg();
//   
//   /* wait a couple of us before sending the first temperature, so the TempRx is ready */
//   for(us_dummyCtr = 0U; us_dummyCtr < 65000U; us_dummyCtr++)
//   {
//      uint16_t a = 0U;
//      a = 50000U;
//      a++;
//		 } // SRO: better to use supported delay functions like nrf_delay_us();...
   
   /* send first temperature */
   appRadioTxSend(halTempGetTemp());
	
	/* Initialise le watchdog */
	 halWDTInit(&s_wdtConf_s);
}

