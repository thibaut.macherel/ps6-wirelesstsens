/**
 * @file    app_radio_rx.h
 * @author  Jan Grossrieder
 * @date    November, 2014
 * @brief   Handles the radio Rx function
 *
 * This module handles the receiving function of the radio. The radio peripheric
 * (@ref hal_radio.h) will be initialized with the requested values (such as radio address,
 * payload size etc.) and the data can be received. It has also a functionality to enter the
 * registration mode. Only in this mode, the radio accepts registration frames.
 */

#ifndef APP_RADIO_RX_H_INCLUDED
#define APP_RADIO_RX_H_INCLUDED

/* includes that must be known for this header -----------------------------------------------*/
#include <stdint.h>
#include <stdbool.h>

/*---------------------------------------------------------------------------------------------+
|  functions prototypes                                                                        |
+---------------------------------------------------------------------------------------------*/
/**
 * Initialize the radio. This function initializes also the HAL radio module (@ref hal_radio).
 * Some configuration data of radio is stored in @ref comm_radio.
 */
void appRadioRxInit(void);

/**
 * Start receiving session.
 */
void appRadioRxRecStart(void);

/**
 * Stop receiving session.
 */
void appRadioRxRecStop(void);

/**
 * Checks if a new temperature is received.
 *
 * @param ub_txDev_p the requested tx member
 * @return true if new temperature is available, false if not
 */
bool appRadioRxIsNewTempAvailable(uint8_t ub_txDev_p);

/**
 * Gets the actual temperature of the requested tx member.
 *
 * @param ub_txDev_p the requested tx member
 * @return the temperature signed in 0.25 degrees per bit
 */
int16_t appRadioRxGetTemp(uint8_t ub_txDev_p);

/**
 * Start registration session. By calling this function, the radio accepts registration data
 * until the function @ref appRadioRxRegisterDeviceStart is called.
 *
 * @param ub_txDev_p the requested tx member
 */
void appRadioRxRegisterDeviceStart(uint8_t ub_txDev_p);

/**
 * Stop registration session.
 */
void appRadioRxRegisterDeviceStop(void);

/**
 * Check if the requested tx member is registered.
 *
 * @param ub_decIdx_p the requested tx member
 * @return true if registered, false if not
 */
bool appRadioIsRegistered(uint8_t ub_decIdx_p);

#endif
