/**
 * @file    main_rx.c
 * @author  Jan Grossrieder
 * @date    November, 2014
 * @brief   Main module of the TempRx which contains the "int main()" function
 */

/* headers for this module -------------------------------------------------------------------*/
#include "app_radio_rx.h"
#include "app_scheduler.h"
#include "hal_clock.h"
#ifdef DEBUG_INFORMATION
#include "simple_uart.h"
#include "boards.h"
#endif

/* standard headers --------------------------------------------------------------------------*/
#include <stdint.h>
#include <stdio.h>
#include <stdbool.h>
//
#include "app_dac.h"

/*---------------------------------------------------------------------------------------------+
|  module global variables                                                                     |
+---------------------------------------------------------------------------------------------*/
/** clock configuration, HF clock on, frequency 16MHz, LF clock on */
static const s_clockConfig_t s_clockConf_s = {true, CLOCK_HF_FREQUENCY_16MHZ, true};

/*---------------------------------------------------------------------------------------------+
|  main functions                                                                              |
+---------------------------------------------------------------------------------------------*/
/**
 * Function for application main entry.
 *
 * @return return is never reached
 */
int main(void)
{
   halClockInit(&s_clockConf_s);
   
   #ifdef DEBUG_INFORMATION
   simple_uart_config(RTS_PIN_NUMBER, TX_PIN_NUMBER, CTS_PIN_NUMBER, RX_PIN_NUMBER, false);
   #endif
   
   appRadioRxInit();
   appSchedulerInit();
   
   appRadioRxRecStart();
   
   while(true)
   {
      appSchedulerRun();
   }
}
