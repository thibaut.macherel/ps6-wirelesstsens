/**
 * @file    hal_radio.h
 * @author  Jan Grossrieder
 * @date    November, 2014
 * @brief   Handles the radio peripheric, some configurations all directly made in this module
 */

#ifndef HAL_RADIO_H_INCLUDED
#define HAL_RADIO_H_INCLUDED

/* includes that must be known for this header -----------------------------------------------*/
#include <stdint.h>
#include <stdbool.h>
#include "nrf.h"

/*---------------------------------------------------------------------------------------------+
|  global constants, macros and enums                                                          |
+---------------------------------------------------------------------------------------------*/
/* Hardware Access Layer defines */
#define RADIO_FREQUENCY    7UL                        /**< Frequency bin 7, 2407MHz */
#define RADIO_MODE         RADIO_MODE_MODE_Ble_1Mbit  /**< Bluetooth low Energy */
#define RADIO_ADD_PREFIX0  0xC4C3C2E7UL               /**< Address prefix0 */
#define RADIO_ADD_PREFIX1  0xC5C6C7C8UL               /**< Address prefix1 */
#define RADIO_ADD_BASE0    0xE7E7E7E7UL               /**< Address base0 */
#define RADIO_ADD_BASE1    0x00C2C2C2UL               /**< Address base1 */

#define RADIO_PKT0_S1_SIZE             0UL  /**< S1 size in bits */
#define RADIO_PKT0_S0_SIZE             0UL  /**< S0 size in bits */
#define RADIO_PKT0_PAYLOAD_SIZE        0UL  /**< payload size in bits */
#define RADIO_PKT1_BASE_ADDRESS_LENGTH 4UL  /**< base address length in bytes */

#define RADIO_PKT1_WHITEEN             RADIO_PCNF1_WHITEEN_Disabled  /**< whiteen */
#define RADIO_PKT1_ENDIAN              RADIO_PCNF1_ENDIAN_Big        /**< endian type */

#define RADIO_CRC_NBR_BITS    RADIO_CRCCNF_LEN_Two    /**< length of CRC bits, 1 or 2 */

/* CRC poly: x^8+x^2+x^1+1 */
#define RADIO_CRC_POLY_1_BIT  0x107UL    /**< polynom in case of 1 bit length */

/* CRC poly: x^16+x^12+x^5+1 */
#define RADIO_CRC_POLY_2_BIT  0x11021UL  /**< polynom in case of 2 bit length */

/**
 * Radio Tx power
 */
typedef enum
{
   RADIO_TXPOWER_POS_4DBM = RADIO_TXPOWER_TXPOWER_Pos4dBm,     /**< +4dBm */
   RADIO_TXPOWER_0DBM = RADIO_TXPOWER_TXPOWER_0dBm,            /**< 0dBm */
   RADIO_TXPOWER_NEG_4DBM = RADIO_TXPOWER_TXPOWER_Neg4dBm,     /**< -4dBm */
   RADIO_TXPOWER_NEG_8DBM = RADIO_TXPOWER_TXPOWER_Neg8dBm,     /**< -8dBm */
   RADIO_TXPOWER_NEG_12DBM = RADIO_TXPOWER_TXPOWER_Neg12dBm,   /**< -12dBm */
   RADIO_TXPOWER_NEG_16DBM = RADIO_TXPOWER_TXPOWER_Neg16dBm,   /**< -16dBm */
   RADIO_TXPOWER_NEG_20DBM = RADIO_TXPOWER_TXPOWER_Neg20dBm,   /**< -20dBm */
   RADIO_TXPOWER_NEG_30DBM = RADIO_TXPOWER_TXPOWER_Neg30dBm    /**< -30dBm */
} e_radioTxPower_t;

/**
 * Radio configuration struct
 */
typedef struct
{
   e_radioTxPower_t e_txPower;   /**< Tx power */
   uint32_t ul_adress;           /**< radio address */
   uint8_t  ub_payloadSize;      /**< payload size in bytes */
} s_radioConfig_t;

/**
 * Callback type of radio Rx interrupt
 */
typedef void t_radioRegCb(void);

/*---------------------------------------------------------------------------------------------+
|  functions prototypes                                                                        |
+---------------------------------------------------------------------------------------------*/
/**
 * Initialize the radio peripherie
 *
 * @param p_config_p the configuration
 * @param ul_payloadPtr_p the address where the data is read/stored as a 32-bit value
 */
void halRadioInit(const s_radioConfig_t *const p_config_p, uint32_t ul_payloadPtr_p);

/**
 * Start a tx session
 */
void halRadioSendStart(void);

/**
 * Wait until the transmission is completed
 * @note This function is blocking.
 */
void halRadioWaitEnd(void);

/**
 * Start the rx session. The radio will listen until the rx session is stoped by calling the
 * function @ref halRadioRecStart.
 */
void halRadioRecStart(void);

/**
 * Stop the rx session
 */
void halRadioRecStop(void);

/**
 * Register a function which should be called in case of a rx interrupt
 *
 * @param p_cbFunc_p the functions address
 */
void halRadioRecRegisterCb(t_radioRegCb *p_cbFunc_p);

/**
 * Power off the radio peripherie
 *
 * @note When powering down, all registers are set to their default value!
 */
void halRadioPowerOff(void);

/**
 * Power on the radio peripherie
 */
void halRadioPowerOn(void);

#endif
