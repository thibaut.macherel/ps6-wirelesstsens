/**
 * @file    hal_spi.c
 * @author  Jan Grossrieder
 * @date    November, 2014
 * @brief   see hal_spi.h
 */

/* headers for this module -------------------------------------------------------------------*/
#include "hal_spi.h"
#include "nrf.h"

/* standard headers (ANSI- and Posix standards) ----------------------------------------------*/
#include <stdint.h>
#include <stdbool.h>
#include <stdio.h>

/*---------------------------------------------------------------------------------------------+
|  module global constants, macros and enums                                                   |
+---------------------------------------------------------------------------------------------*/
/**
 * SPI data structure
 */
typedef struct
{
   uint8_t *p_bufAddr;        /**< address of buffer */
   uint16_t ui_length;        /**< length of data */
   uint16_t ui_dataIdxCtr;    /**< data index counter */
   uint16_t ui_dataSendCtr;   /**< data send counter */
   bool isBusy;
} s_spiBuf_t;

/*---------------------------------------------------------------------------------------------+
|  module global variables                                                                     |
+---------------------------------------------------------------------------------------------*/
/**
 * The current spi data
 */
static s_spiBuf_t s_spiBufTx_s = {0};

/**
 * Chip select pi (0...32)
 */
static uint8_t ub_csPin_s = 0U;

/*---------------------------------------------------------------------------------------------+
|  functions                                                                                   |
+---------------------------------------------------------------------------------------------*/
void halSPIInit(const s_spiConfig_t *const p_config_p)
{
   ub_csPin_s = p_config_p->ui_pinCS;
   
   /* set clock, CS and MOSI as Output */
   NRF_GPIO->DIRSET = (1UL << p_config_p->ui_pinClk);
   NRF_GPIO->DIRSET = (1UL << p_config_p->ui_pinCS);
   NRF_GPIO->DIRSET = (1UL << p_config_p->ui_pinMOSI);
   
   /* set CS to high */
   NRF_GPIO->OUTSET = (1UL << p_config_p->ui_pinCS);
   
   /* set MISO as Input */
   NRF_GPIO->DIRCLR = (1UL << p_config_p->ui_pinMISO);
   
   /* enable input buffer for all three pins */
   NRF_GPIO->PIN_CNF[p_config_p->ui_pinClk] &= ~GPIO_PIN_CNF_INPUT_Msk;
   NRF_GPIO->PIN_CNF[p_config_p->ui_pinClk] |= 
      (GPIO_PIN_CNF_INPUT_Connect << GPIO_PIN_CNF_INPUT_Pos);
   NRF_GPIO->PIN_CNF[p_config_p->ui_pinMOSI] &= ~GPIO_PIN_CNF_INPUT_Msk;
   NRF_GPIO->PIN_CNF[p_config_p->ui_pinMOSI] |= 
      (GPIO_PIN_CNF_INPUT_Connect << GPIO_PIN_CNF_INPUT_Pos);
   NRF_GPIO->PIN_CNF[p_config_p->ui_pinMISO] &= ~GPIO_PIN_CNF_INPUT_Msk;
   NRF_GPIO->PIN_CNF[p_config_p->ui_pinMISO] |= 
      (GPIO_PIN_CNF_INPUT_Connect << GPIO_PIN_CNF_INPUT_Pos);
   
   /* select pins for SPI in SPI Register */
   NRF_SPI0->PSELSCK = p_config_p->ui_pinClk;
   NRF_SPI0->PSELMOSI = p_config_p->ui_pinMOSI;
   NRF_SPI0->PSELMISO = p_config_p->ui_pinMISO;
   
   NRF_SPI0->FREQUENCY = (p_config_p->e_freq << SPI_FREQUENCY_FREQUENCY_Pos);
   NRF_SPI0->CONFIG = (p_config_p->e_polarity << SPI_CONFIG_CPOL_Pos);
   NRF_SPI0->CONFIG = (p_config_p->e_phase << SPI_CONFIG_CPHA_Pos);
   NRF_SPI0->CONFIG = (p_config_p->e_order << SPI_CONFIG_ORDER_Pos);
   
   /* enable interrupt with middle priority */
   NVIC_SetPriority(SPI0_TWI0_IRQn, 2U);
   NVIC_EnableIRQ(SPI0_TWI0_IRQn);
   
   /* enable ready interrupt */
   NRF_SPI0->INTENSET = (SPI_INTENSET_READY_Enabled << SPI_INTENSET_READY_Pos);
   
   /* enable SPI */
   NRF_SPI0->ENABLE = (SPI_ENABLE_ENABLE_Enabled << SPI_ENABLE_ENABLE_Pos);
}

void halSPISendStart(const uint8_t *const ub_data_p, uint16_t ui_length_p)
{
   if((ub_data_p != NULL) && (ui_length_p > 0U))
   {
      /* set CS to low */
      NRF_GPIO->OUTCLR = (1UL << ub_csPin_s);
      
      s_spiBufTx_s.p_bufAddr = (uint8_t *)ub_data_p;
      s_spiBufTx_s.ui_length = ui_length_p;
      s_spiBufTx_s.ui_dataIdxCtr = 0U;
      s_spiBufTx_s.ui_dataSendCtr = 0U;
      s_spiBufTx_s.isBusy = true;
      
      /* two bytes can be written at the same time */
      NRF_SPI0->TXD = s_spiBufTx_s.p_bufAddr[s_spiBufTx_s.ui_dataIdxCtr++];
      if(s_spiBufTx_s.ui_length > 1U)
      {
         NRF_SPI0->TXD = s_spiBufTx_s.p_bufAddr[s_spiBufTx_s.ui_dataIdxCtr++];
      }
      
      /* ready for receiving */
      NRF_SPI0->EVENTS_READY = 0U;
   }
}

bool halSPIisBusy(void)
{
   return s_spiBufTx_s.isBusy;
}

/*---------------------------------------------------------------------------------------------+
|  Interrupts                                                                                  |
+---------------------------------------------------------------------------------------------*/
/**
 * SPI0 IRQ handler
 */
void SPI0_TWI0_IRQHandler(void)
{
   uint8_t ub_dummy = NRF_SPI0->RXD;
   
   /* byte sent, increment counter */
   s_spiBufTx_s.ui_dataSendCtr++;
   
   /* is there one more byte to send? */
   if(s_spiBufTx_s.ui_dataSendCtr < s_spiBufTx_s.ui_length)
   {
      /* as there are two bytes sent at the same time, do not care about the first */
      if(s_spiBufTx_s.ui_dataIdxCtr < s_spiBufTx_s.ui_length)
      {
         NRF_SPI0->TXD = s_spiBufTx_s.p_bufAddr[s_spiBufTx_s.ui_dataIdxCtr++];
      }
   }
   else
   {
      /* set CS back to high, end transmitting */
      NRF_GPIO->OUTSET = (1UL << ub_csPin_s);
      s_spiBufTx_s.isBusy = false;
   }
   
   /* dummy inctruction - ready to receive next byte */
   NRF_SPI0->EVENTS_READY = 0U;
}
