/**
 * @file    hal_gpio.c
 * @author  Jan Grossrieder
 * @date    November, 2014
 * @brief   see hal_gpio.h
 */

/* headers for this module -------------------------------------------------------------------*/
#include "hal_gpio.h"
#include "nrf.h"

/* standard headers (ANSI- and Posix standards) ----------------------------------------------*/
#include <stdint.h>
#include <stdbool.h>
#include <stdio.h>

/*---------------------------------------------------------------------------------------------+
|  module global variables                                                                     |
+---------------------------------------------------------------------------------------------*/
/**
 * the registered callback functions (up to 4)
 */
static t_gpioCb *gpioCallbackFunc_s[4U] = {NULL};

/*---------------------------------------------------------------------------------------------+
|  functions                                                                                   |
+---------------------------------------------------------------------------------------------*/
void halGPIOInitPin(const s_gpioConfig_t *const p_config_p)
{
   if(p_config_p->e_pinDir == GPIO_PIN_OUTPUT)
   {
      NRF_GPIO->DIRSET = (1UL << p_config_p->ui_pin);
      
      if(p_config_p->e_pinLevel == GPIO_PIN_HIGH)
      {
         NRF_GPIO->OUTSET = (1UL << p_config_p->ui_pin);
      }
      else
      {
         NRF_GPIO->OUTCLR = (1UL << p_config_p->ui_pin);
      }
   }
   else
   {
      NRF_GPIO->DIRCLR = (1UL << p_config_p->ui_pin);
      /* set pullup, pulldown, enable input */
      NRF_GPIO->PIN_CNF[p_config_p->ui_pin] &= ~GPIO_PIN_CNF_PULL_Msk;
      NRF_GPIO->PIN_CNF[p_config_p->ui_pin] |= 
         (p_config_p->e_pinPull << GPIO_PIN_CNF_PULL_Pos);
      NRF_GPIO->PIN_CNF[p_config_p->ui_pin] &= ~GPIO_PIN_CNF_INPUT_Msk;
      NRF_GPIO->PIN_CNF[p_config_p->ui_pin] |= 
         (GPIO_PIN_CNF_INPUT_Connect << GPIO_PIN_CNF_INPUT_Pos);
   }
}

void halGPIOInitInterrupt(const s_gpioIntConfig_t *const p_config_p)
{
   static uint8_t ub_intIdxCnt_s = 0U;
   
   if(ub_intIdxCnt_s < 4U)
   {
      /* enable interrupt with middle priority */
      NVIC_SetPriority(GPIOTE_IRQn, 3U);
      NVIC_EnableIRQ(GPIOTE_IRQn);
      
      NRF_GPIOTE->INTENSET = (1U << ub_intIdxCnt_s);
      NRF_GPIOTE->CONFIG[ub_intIdxCnt_s] = 
         (GPIOTE_CONFIG_MODE_Event << GPIOTE_CONFIG_MODE_Pos) | 
         (p_config_p->ui_pin << GPIOTE_CONFIG_PSEL_Pos) |
         (p_config_p->e_polarity << GPIOTE_CONFIG_POLARITY_Pos);
      NRF_GPIOTE->POWER = (GPIOTE_POWER_POWER_Enabled << GPIOTE_POWER_POWER_Pos);
      
      /* register callback funtion */
      gpioCallbackFunc_s[ub_intIdxCnt_s] = p_config_p->cbFunc;
      
      ub_intIdxCnt_s++;
   }
}

void halGPIOPinSet(uint8_t ub_pin_p, e_gpioLevel_t e_level_p)
{
   if(e_level_p == GPIO_PIN_HIGH)
   {
      NRF_GPIO->OUTSET = (1U << ub_pin_p);
   }
   else
   {
      NRF_GPIO->OUTCLR = (1U << ub_pin_p);
   }
}

e_gpioLevel_t halGPIOPinGet(uint8_t ub_pin_p)
{
   return (e_gpioLevel_t)((NRF_GPIO->IN >> ub_pin_p) & 0x01);
}

/*---------------------------------------------------------------------------------------------+
|  Interrupts                                                                                  |
+---------------------------------------------------------------------------------------------*/
/**
 * GPIOTE IRQ handler
 */
void GPIOTE_IRQHandler(void)
{
   uint8_t ub_ctr;
   
   for(ub_ctr = 0U; ub_ctr < 4U; ub_ctr++)
   {
      if((NRF_GPIOTE->EVENTS_IN[ub_ctr] == 1UL) && (gpioCallbackFunc_s[ub_ctr] != NULL))
      {
         /* call callbcak function */
         gpioCallbackFunc_s[ub_ctr]();
         
         /* clear event */
         NRF_GPIOTE->EVENTS_IN[ub_ctr] = 0UL;
      }
   }
}
