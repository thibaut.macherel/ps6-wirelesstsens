/**
 * @file    hal_factory.h
 * @author  Jan Grossrieder
 * @date    November, 2014
 * @brief   Read out the unique device ID from the chip factory register
 */

#ifndef HAL_FACTORY_H_INCLUDED
#define HAL_FACTORY_H_INCLUDED

/* includes that must be known for this header -----------------------------------------------*/
#include <stdint.h>

/*---------------------------------------------------------------------------------------------+
|  function prototypes                                                                         |
+---------------------------------------------------------------------------------------------*/
/**
 * Gets the unique 64-Bit device ID of the FICR register.
 *
 * @return Address of the Device ID (LSB)
 */
const uint32_t *halFactoryGetDeviceID(void);

#endif
