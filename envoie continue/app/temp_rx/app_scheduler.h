/**
 * @file    app_scheduler.h
 * @author  Jan Grossrieder
 * @date    November, 2014
 * @brief   Scheduler of the tempRx
 */

#ifndef APP_SCHEDULER_H_INCLUDED
#define APP_SCHEDULER_H_INCLUDED

/* includes that must be known for this header -----------------------------------------------*/
#include <stdint.h>
#include <stdbool.h>

/*---------------------------------------------------------------------------------------------+
|  functions prototypes                                                                        |
+---------------------------------------------------------------------------------------------*/
/**
 * Initialize the scheduler. This function initializes also all relied hal module, such as @ref
 * hal_rtc and @ref hal_gpio.
 */
void appSchedulerInit(void);

/**
 * Run tasks, should be called contionously.
 */
void appSchedulerRun(void);

#endif

/*---------------------------------------------------------------------------------------------+
|  detailed doxygen description                                                                |
+---------------------------------------------------------------------------------------------*/
/**
 * @page app_schedRx_desc TempRx Scheduler
 * The TempRx schedulers main task is to wait for a new datapack and according to its value
 * set the DAC outputs over SPI on the extensionboard. It has a run-tasks which should be called
 * cyclic.<br>
 * This module handles also the RTC and the GPIO. By pressing a button (button0 or button1), the
 * module changes into registration mode for a certain time defined in @ref RADIO_REG_TIME_IN_MS.
 * The registration state is shown by flashing LEDs.
 */
