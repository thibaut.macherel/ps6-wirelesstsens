/**
 * @file    app_scheduler.h
 * @author  Jan Grossrieder
 * @date    November, 2014
 * @brief   Scheduler of the tempTx
 */

#ifndef APP_SCHEDULER_TX_H_INCLUDED
#define APP_SCHEDULER_TX_H_INCLUDED

/* includes that must be known for this header -----------------------------------------------*/
#include <stdint.h>
#include <stdbool.h>

/*---------------------------------------------------------------------------------------------+
|  functions prototypes                                                                        |
+---------------------------------------------------------------------------------------------*/
/**
 * Initialize the scheduler. This function initializes also all relied hal module, such as @ref
 * hal_rtc.h
 */
void appSchedulerInit(void);

#endif

/*---------------------------------------------------------------------------------------------+
|  detailed doxygen description                                                                |
+---------------------------------------------------------------------------------------------*/
/**
 * @page app_schedTx_desc TempTx Scheduler
 * The TempTx schedulers main task is to register a callback to the RTC which is called every
 * one minute. In this callback the TempTx reads the silicon temperature and sends it over BLE
 * to the TempRx. In the init function there is also the registration datapack sent. As the
 * init function is called at a startup of the chip, the registration is done at this point.
 * Once the TempTx is started up, it can't send another registration unless it will do a reset.
 */
