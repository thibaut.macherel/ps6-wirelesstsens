/**
 * @file    app_radio_tx.h
 * @author  Jan Grossrieder
 * @date    November, 2014
 * @brief   Handles the radio Tx function
 *
 * This module handles the transieving function of the radio. The radio peripheric
 * (@ref hal_radio.h) will be initialized with the requested values (such as tx power, radio
 * address etc.) and the measured temperature can then be sent.
 */

#ifndef APP_RADIO_TX_H_INCLUDED
#define APP_RADIO_TX_H_INCLUDED

/* includes that must be known for this header -----------------------------------------------*/
#include <stdint.h>
#include <stdbool.h>

/*---------------------------------------------------------------------------------------------+
|  functions prototypes                                                                        |
+---------------------------------------------------------------------------------------------*/
/**
 * Initialize the radio. This function initializes also the HAL radio module (@ref hal_radio).
 * Some configuration data of radio is stored in @ref comm_radio.
 */
void appRadioTxInit(void);

/**
 * Send out a packet with the temperature.
 *
 * @param si_temp_p the temperature in 0.25 degrees per bit
 */
void appRadioTxSend(int16_t si_temp_p);

/**
 * Send out a packet with the temperature and a RSSI request
 *
 * @param si_temp_p the temperature in 0.25 degrees per bit
 */
void appRadioTxSendRSSI(int16_t si_temp_p);

/**
 * Send out a registration packet. This packet has some special data that indicates this is a
 * registration.
 */
void appRadioTxSendReg(void);

#endif
