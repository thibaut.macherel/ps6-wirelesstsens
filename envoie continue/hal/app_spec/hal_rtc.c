/**
 * @file    hal_rtc.c
 * @author  Jan Grossrieder
 * @date    November, 2014
 * @brief   see hal_rtc.h
 */

/* headers for this module -------------------------------------------------------------------*/
#include "hal_rtc.h"
#include "nrf.h"

/* standard headers (ANSI- and Posix standards) ----------------------------------------------*/
#include <stdint.h>
#include <stdbool.h>
#include <stdio.h>

/*---------------------------------------------------------------------------------------------+
|  module global variables                                                                     |
+---------------------------------------------------------------------------------------------*/
/**
 * The registered callback function
 */
static t_rtcCompCb *rtcCallbackFunc_s = NULL;

/*---------------------------------------------------------------------------------------------+
|  functions                                                                                   |
+---------------------------------------------------------------------------------------------*/
void halRTCInit(const s_rtcConfig_t *const p_config_p)
{
   /* power on RTC peripherie */
   NRF_RTC0->POWER = (RTC_POWER_POWER_Enabled << RTC_POWER_POWER_Pos);
   
   NRF_RTC0->PRESCALER = p_config_p->ui_prescaler;
   
   if(p_config_p->isCompareIntEnabled == true)
   {
      /* enable interrupt with middle priority */
      NVIC_SetPriority(RTC0_IRQn, 2U);
      NVIC_EnableIRQ(RTC0_IRQn);
      
      /* set compare value */
      NRF_RTC0->CC[0U] = p_config_p->ul_compareVal;
      
      /* enable interrupt on compare event */
      NRF_RTC0->INTENSET = 
         (RTC_INTENSET_COMPARE0_Enabled << RTC_INTENSET_COMPARE0_Pos);
      NRF_RTC0->EVTENSET = (RTC_EVTENSET_COMPARE0_Enabled << RTC_EVTENSET_COMPARE0_Pos);
      
      NRF_RTC0->EVENTS_COMPARE[0U] = 0UL;
   }
   
   NRF_RTC0->TASKS_START = 1UL;
}

void halRTCRegisterCb(t_rtcCompCb *p_cbFunc_p)
{
   rtcCallbackFunc_s = p_cbFunc_p;
}

/*---------------------------------------------------------------------------------------------+
|  Interrupts                                                                                  |
+---------------------------------------------------------------------------------------------*/
/**
 * RTC0 IRQ handler
 */
void RTC0_IRQHandler(void)
{
   if(rtcCallbackFunc_s != NULL)
   {
      rtcCallbackFunc_s();
   }
   
   /* clear counter */
   NRF_RTC0->TASKS_CLEAR = 1UL;
   /* clear events */
   NRF_RTC0->EVENTS_COMPARE[0U] = 0UL;
}
