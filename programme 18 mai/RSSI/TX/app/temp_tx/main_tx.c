/**
 * @file    main_tx.c
 * @author  Jan Grossrieder
 * @date    November, 2014
 * @brief   Main module of the TempTx which contains the "int main()" function
 */

/* headers for this module -------------------------------------------------------------------*/
#include "app_radio_tx.h"
#include "app_scheduler.h"
#include "hal_clock.h"
#include "hal_power.h"

/* standard headers --------------------------------------------------------------------------*/
#include <stdint.h>
#include <stdio.h>
#include <stdbool.h>
#ifdef DEBUG_INFORMATION
#include "simple_uart.h"
#endif

/*---------------------------------------------------------------------------------------------+
|  local constants and macros                                                                  |
+---------------------------------------------------------------------------------------------*/
#ifdef DEBUG_INFORMATION
#define RTS_PIN_NUMBER		8
#define TX_PIN_NUMBER			9
#define CTS_PIN_NUMBER		10
#define RX_PIN_NUMBER			11
#define HWFC							false
#endif

/*---------------------------------------------------------------------------------------------+
|  module global variables                                                                     |
+---------------------------------------------------------------------------------------------*/
/** clock configuration, HF clock on, frequency 16MHz, LF clock on */
static const s_clockConfig_t s_clockConf_s = {true, CLOCK_HF_FREQUENCY_16MHZ, true};

/** Power RAM config */
static const s_powerConfig_t spowerRamConf_s =
   {true, true,   /**< RAM0: On, Retetion On */
   false, false,  /**< RAM1: Off, Retetion Off */
   false, false,  /**< RAM2: Off, Retetion Off */
   false, false}; /**< RAM3: Off, Retetion Off */

/*---------------------------------------------------------------------------------------------+
|  main functions                                                                              |
+---------------------------------------------------------------------------------------------*/
/**
 * Function for application main entry.
 *
 * @return return is never reached
 */
int main(void)
{
	halClockInit(&s_clockConf_s);
	
	 #ifdef DEBUG_INFORMATION
   simple_uart_config(RTS_PIN_NUMBER, TX_PIN_NUMBER, CTS_PIN_NUMBER, RX_PIN_NUMBER, HWFC);
	 simple_uart_putstring((uint8_t *)"START_PROGRAM   ");
   #endif
	
   appRadioTxInit();
   appSchedulerInit();
   
   /* disable all unnecessary RAM Blocks */
   halPowerRAMSelect(&spowerRamConf_s);
   
   while(true)
   {
      /* Always go back to sleep mode */
      halPowerEnterSleepMode();
   }
}
