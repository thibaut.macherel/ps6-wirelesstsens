/**
 * @file    app_radio_tx.c
 * @author  Jan Grossrieder
 * @date    November, 2014
 * @brief   see app_radio_tx.h
 */

/* headers for this module -------------------------------------------------------------------*/
#include "app_radio_tx.h"
#include "hal_radio.h"
#include "hal_factory.h"
#include "app_radio_conf.h"
#ifdef DEBUG_INFORMATION
#include "simple_uart.h"
#endif

/* standard headers --------------------------------------------------------------------------*/
#include <stdint.h>
#include <stdbool.h>
#include <stdio.h>

/*---------------------------------------------------------------------------------------------+
|  local constants and macros                                                                  |
+---------------------------------------------------------------------------------------------*/
#define RSSI_max  80    /**< valeur = -valeur[dbm], valeur maximum du RSSI sans modification de la puissance d'�mission */
#define RSSI_min  70    /**< valeur minimum du RSSI sans modification de la puissance d'�mission */

/*---------------------------------------------------------------------------------------------+
|  module global variables                                                                     |
+---------------------------------------------------------------------------------------------*/
/** Transmit packet buffer */
static uint8_t ub_packet_s[RADIO_PAYLOAD_SIZE] = {0U};

/** Copy of the device ID in unint8_t */
static uint8_t ub_packetID_s[8] = {0U};

/** radio configuration */
static s_radioConfig_t s_radioConf_s = {RADIO_TXPOWER_0DBM, false, RADIO_ADDRESS, RADIO_PAYLOAD_SIZE};

/** RSSI is receiving */
static bool isRSSIrx = false;

/*---------------------------------------------------------------------------------------------+
|  local functions prototypes                                                                  |
+---------------------------------------------------------------------------------------------*/
/**
 * This function copies an 64-Bit value (e. g. device ID) stored in a 2*32 bit array into a
 * 4*8bit array.
 *
 * @param p_src_p address of source
 * @param p_dest_p address of destination
 */
static void copyDeviceID32To8(const uint32_t *const p_src_p, uint8_t *p_dest_p);

/**
 * function to change the registrer where the radio config power is write, a call of the function halRadioInit is needed
 * for the change to be effective
 */
static void PowerChange(bool);

/**
 * Callback function which is called by changing from TX to RX mode.
 */
static void radioDisableEventCallback(void);


/*---------------------------------------------------------------------------------------------+
|  functions                                                                                   |
+---------------------------------------------------------------------------------------------*/
void appRadioTxInit(void)
{
   /* store the device ID */
   copyDeviceID32To8(halFactoryGetDeviceID(), &ub_packet_s[RADIO_PAYLOAD_ID_IDX]);
	
	 /* store the device ID */
   copyDeviceID32To8(halFactoryGetDeviceID(), &ub_packetID_s[0]);
   
   /* normally functional data */
   ub_packet_s[RADIO_PAYLOAD_REG_IDX] = RADIO_NOR_FUNCT_DATA;
   
   /* Set radio configuration parameters */
   halRadioInit(&s_radioConf_s, (uint32_t)ub_packet_s);
	
	    /* set high priority */
   NVIC_SetPriority(RADIO_IRQn, 1);
   NVIC_EnableIRQ(RADIO_IRQn);
   
   /* enable interrupt for end */
   NRF_RADIO->INTENSET |= (RADIO_INTENSET_END_Enabled << RADIO_INTENSET_END_Pos);
   
   /* clear events */
   NRF_RADIO->EVENTS_READY = 0U;
   NRF_RADIO->EVENTS_END = 0U;
   NRF_RADIO->EVENTS_DISABLED = 0U;
   
   /* power off radio */
   halRadioPowerOff();
}

void appRadioTxSend(int16_t si_temp_p)
{
   ub_packet_s[RADIO_PAYLOAD_DATA_IDX] = (uint8_t)((si_temp_p >> 8U) & 0xFFU);
   ub_packet_s[RADIO_PAYLOAD_DATA_IDX + 1U] = (uint8_t)(si_temp_p & 0xFFU);
   
   /* power on the radio */
   halRadioPowerOn();
   halRadioInit(&s_radioConf_s, (uint32_t)ub_packet_s);
   
   halRadioSendStart();
   
   /* wait for end */
   halRadioWaitEnd();
   
   /* and power off again */
   halRadioPowerOff();
}

void appRadioTxSendRSSI(int16_t si_temp_p)
{	
	 ub_packet_s[RADIO_PAYLOAD_REG_IDX] = RADIO_RSSI_REQUEST;
   ub_packet_s[RADIO_PAYLOAD_DATA_IDX] = (uint8_t)((si_temp_p >> 8U) & 0xFFU);
   ub_packet_s[RADIO_PAYLOAD_DATA_IDX + 1U] = (uint8_t)(si_temp_p & 0xFFU);
   
   /* power on the radio */
   halRadioPowerOn();
   halRadioInit(&s_radioConf_s, (uint32_t)ub_packet_s);
   
	 // active le short pour que lorsque l'envoie est termin�, la radio passe en mode r�ception
	 NRF_RADIO->SHORTS |= RADIO_SHORTS_DISABLED_RXEN_Msk;

	 // d�marre l'envoie
	 halRadioSendStart();
	
	 /* enable interrupt for disabled */
   NRF_RADIO->INTENSET |= (RADIO_INTENSET_DISABLED_Enabled << RADIO_INTENSET_DISABLED_Pos);
	
	 // enregistre une fonction de callback
	 halRadioRecRegisterCb(&radioDisableEventCallback);

	 // attends que l'envoie soit termin�
	 while(isRSSIrx != true);
	 isRSSIrx = false;

	 /* disable interrupt for disabled */
	 NRF_RADIO->INTENCLR |= RADIO_INTENCLR_DISABLED_Msk;

	 // attends d'avoir r�ceptionn� le RSSI ou que le temps soit termin�
	 uint32_t temporisation = 0;
	 while(!((NRF_RADIO->EVENTS_END == 1U) | (temporisation > 998)))
	 {
		 temporisation ++;
	 }
	 
	 #ifdef DEBUG_INFORMATION
	 if(NRF_RADIO->CRCSTATUS == 0) simple_uart_putstring((const uint8_t *)"ERROR  ");
	 else simple_uart_putstring((const uint8_t *)"OK     ");
	 #endif
	 
	 bool CRCok = NRF_RADIO->CRCSTATUS;
	 
	 /* and power off again */
   halRadioPowerOff();

	 #ifdef DEBUG_INFORMATION
	 char Temporisation[50] = {'\0'};
	 (void)sprintf(Temporisation, "temporisation %d  ", temporisation);
	 simple_uart_putstring((const uint8_t *)Temporisation);
	 
	 char test[50] = {'\0'};
	 if(s_radioConf_s.e_txPower == RADIO_TXPOWER_POS_4DBM) simple_uart_putstring((const uint8_t *)" +4");
	 else if(s_radioConf_s.e_txPower == RADIO_TXPOWER_0DBM) simple_uart_putstring((const uint8_t *)"  0");
	 else if(s_radioConf_s.e_txPower == RADIO_TXPOWER_NEG_4DBM) simple_uart_putstring((const uint8_t *)" -4");
	 else if(s_radioConf_s.e_txPower == RADIO_TXPOWER_NEG_8DBM) simple_uart_putstring((const uint8_t *)" -8");
	 else if(s_radioConf_s.e_txPower == RADIO_TXPOWER_NEG_12DBM) simple_uart_putstring((const uint8_t *)"-12");
	 else if(s_radioConf_s.e_txPower == RADIO_TXPOWER_NEG_16DBM) simple_uart_putstring((const uint8_t *)"-16");
	 else if(s_radioConf_s.e_txPower == RADIO_TXPOWER_NEG_20DBM) simple_uart_putstring((const uint8_t *)"-20");
	 else if(s_radioConf_s.e_txPower == RADIO_TXPOWER_NEG_30DBM) simple_uart_putstring((const uint8_t *)"-30");
	 simple_uart_putstring((const uint8_t *)"DBM  ");
	 #endif
	 
	 // contr�le que le CRC est OK
	 if(CRCok)
	 {
  		 if(ub_packet_s[RADIO_PAYLOAD_REG_IDX] == RADIO_RSSI_VALUE)
  		 {
				 // contr�le que le ID dans le paquet corresponds � ce ship
			   uint8_t pack_nbr = 0;
  			 bool equal = true;
  			 while(pack_nbr <(RADIO_PAYLOAD_DATA_IDX-1))
  			 {
  				 if(ub_packet_s[pack_nbr+RADIO_PAYLOAD_ID_IDX] == ub_packetID_s[pack_nbr]);
  				 else
					 {
						 equal = false;
					 }

					 pack_nbr ++;
  			 }
  			 if(equal == true)
  			 {
  				 uint16_t RSSIreceived = ((int16_t)ub_packet_s[RADIO_PAYLOAD_DATA_IDX] << 8U) | 
  									 (int16_t)ub_packet_s[RADIO_PAYLOAD_DATA_IDX + 1U];
					 
					 #ifdef DEBUG_INFORMATION
				   (void)sprintf(test, "RSSI_received: %d  ", RSSIreceived);
				   #endif
					 
  				 if(RSSIreceived < RSSI_min)
  				 {
						 PowerChange(false);
  				 }
  				 else if(RSSIreceived > RSSI_max)
  				 {
  					 PowerChange(true);
  				 }
  			 }
				 #ifdef DEBUG_INFORMATION
				 else (void)sprintf(test, "RSSI_received: NO  ");
				 #endif
  		 }
			 else
			 {
				 PowerChange(true);
				 
				 #ifdef DEBUG_INFORMATION
				 (void)sprintf(test, "RSSI_received: NO  ");
				 #endif
			 }
			 
			 #ifdef DEBUG_INFORMATION
			 simple_uart_putstring((const uint8_t *)test);
			 #endif
		 }
	 else // si le CRC est faux
	 {
		 #ifdef DEBUG_INFORMATION
		 simple_uart_putstring((const uint8_t *)"     CRC FALSE     ");
		 #endif
		 PowerChange(true);
	 }
			  	
	 /* restore normal data */
   ub_packet_s[RADIO_PAYLOAD_REG_IDX] = RADIO_NOR_FUNCT_DATA;
	 
	 /* store the device ID */
   copyDeviceID32To8(halFactoryGetDeviceID(), &ub_packet_s[RADIO_PAYLOAD_ID_IDX]);
}


void appRadioTxSendReg(void)
{
   ub_packet_s[RADIO_PAYLOAD_REG_IDX] = RADIO_REGISTRATION_DATA;
   
   /* power on the radio */
   halRadioPowerOn();
   halRadioInit(&s_radioConf_s, (uint32_t)ub_packet_s);
	
	 NVIC_SetPriority(RADIO_IRQn, 1);
   NVIC_EnableIRQ(RADIO_IRQn);
   
   halRadioSendStart();
   
   /* wait for end */
   halRadioWaitEnd();
   
   /* and power off again */
   halRadioPowerOff();
   
   /* restore normal data */
   ub_packet_s[RADIO_PAYLOAD_REG_IDX] = RADIO_NOR_FUNCT_DATA;
}


/*---------------------------------------------------------------------------------------------+
|  local functions                                                                             |
+---------------------------------------------------------------------------------------------*/
static void copyDeviceID32To8(const uint32_t *const p_src_p, uint8_t *p_dest_p)
{
   uint8_t ub_ctr;
   
   for(ub_ctr = 0U; ub_ctr < 2U; ub_ctr++)
   {
      p_dest_p[ub_ctr * 4U] = (p_src_p[ub_ctr] >> 24U) & 0xFFU;
      p_dest_p[(ub_ctr * 4U) + 1U] = (p_src_p[ub_ctr] >> 16U) & 0xFFU;
      p_dest_p[(ub_ctr * 4U) + 2U] = (p_src_p[ub_ctr] >> 8U) & 0xFFU;
      p_dest_p[(ub_ctr * 4U) + 3U] = p_src_p[ub_ctr] & 0xFFU;
   }
}

static void radioDisableEventCallback(void)
{	 
	 NRF_RADIO->SHORTS &= ~(RADIO_SHORTS_DISABLED_RXEN_Msk);
	 isRSSIrx = true;
}

static void PowerChange(bool incr)		// True pour augmenter
{	 
	 if (incr == true)
	 {
		 if(s_radioConf_s.e_txPower == RADIO_TXPOWER_0DBM) s_radioConf_s.e_txPower = RADIO_TXPOWER_POS_4DBM;
		 else if(s_radioConf_s.e_txPower == RADIO_TXPOWER_NEG_4DBM) s_radioConf_s.e_txPower = RADIO_TXPOWER_0DBM;
		 else if(s_radioConf_s.e_txPower == RADIO_TXPOWER_NEG_8DBM) s_radioConf_s.e_txPower = RADIO_TXPOWER_NEG_4DBM;
		 else if(s_radioConf_s.e_txPower == RADIO_TXPOWER_NEG_12DBM) s_radioConf_s.e_txPower = RADIO_TXPOWER_NEG_8DBM;
		 else if(s_radioConf_s.e_txPower == RADIO_TXPOWER_NEG_16DBM) s_radioConf_s.e_txPower = RADIO_TXPOWER_NEG_12DBM;
		 else if(s_radioConf_s.e_txPower == RADIO_TXPOWER_NEG_20DBM) s_radioConf_s.e_txPower = RADIO_TXPOWER_NEG_16DBM;
		 else if(s_radioConf_s.e_txPower == RADIO_TXPOWER_NEG_30DBM) s_radioConf_s.e_txPower = RADIO_TXPOWER_NEG_20DBM;
	 }
	 else
	 {
		 if(s_radioConf_s.e_txPower == RADIO_TXPOWER_POS_4DBM) s_radioConf_s.e_txPower = RADIO_TXPOWER_0DBM;						 
		 else if(s_radioConf_s.e_txPower == RADIO_TXPOWER_0DBM) s_radioConf_s.e_txPower = RADIO_TXPOWER_NEG_4DBM;
		 else if(s_radioConf_s.e_txPower == RADIO_TXPOWER_NEG_4DBM) s_radioConf_s.e_txPower = RADIO_TXPOWER_NEG_8DBM;
		 else if(s_radioConf_s.e_txPower == RADIO_TXPOWER_NEG_8DBM) s_radioConf_s.e_txPower = RADIO_TXPOWER_NEG_12DBM;
		 else if(s_radioConf_s.e_txPower == RADIO_TXPOWER_NEG_12DBM) s_radioConf_s.e_txPower = RADIO_TXPOWER_NEG_16DBM;
		 else if(s_radioConf_s.e_txPower == RADIO_TXPOWER_NEG_16DBM) s_radioConf_s.e_txPower = RADIO_TXPOWER_NEG_20DBM;
		 else if(s_radioConf_s.e_txPower == RADIO_TXPOWER_NEG_20DBM) s_radioConf_s.e_txPower = RADIO_TXPOWER_NEG_30DBM;
	 }
}
