/**
 * @file    app_scheduler.c
 * @author  Jan Grossrieder
 * @date    November, 2014
 * @brief   see @ref app_schedTx_desc
 */

/* headers for this module -------------------------------------------------------------------*/
#include "app_scheduler.h"
#include "app_radio_tx.h"
#include "hal_rtc.h"
#include "hal_temp.h"
#include "hal_power.h"
#ifdef DEBUG_INFORMATION
#include "simple_uart.h"
#endif

/* standard headers --------------------------------------------------------------------------*/
#include <stdint.h>
#include <stdbool.h>
#include <stdio.h>

/*---------------------------------------------------------------------------------------------+
|  local constants and macros                                                                  |
+---------------------------------------------------------------------------------------------*/
#define RTC_STEP_IN_MS  125UL    /**< RTC Step in ms, it will be used to calculate the prescaler */
//#define RTC_STEP_IN_MS  10UL    /**< RTC Step in ms, it will be used to calculate the prescaler */
#define RTC_INT_IN_MS   1000UL  /**< RTC Intervall time in ms, used to calculate the comp value */
//#define RTC_PRESCALER   (((32768UL * RTC_STEP_IN_MS) / 1000UL) - 1UL)
#define RTC_PRESCALER   0
//#define RTC_COMP_VAL    (RTC_INT_IN_MS / RTC_STEP_IN_MS)
#define RTC_COMP_VAL    10000

#define RSSI_AJUST_PACK_NBR 0 /** Number of packet before a RSSI request */

/*---------------------------------------------------------------------------------------------+
|  local functions prototypes                                                                  |
+---------------------------------------------------------------------------------------------*/
/**
 * This function is called on every RTC compare event.
 */
static void rtcCallback(void);

/*---------------------------------------------------------------------------------------------+
|  module global variables                                                                     |
+---------------------------------------------------------------------------------------------*/
/** RTC configuration */
static const s_rtcConfig_t s_rtcConf_s = {RTC_PRESCALER, true, RTC_COMP_VAL};

/*---------------------------------------------------------------------------------------------+
|  functions                                                                                   |
+---------------------------------------------------------------------------------------------*/
void appSchedulerInit(void)
{
//   uint16_t us_dummyCtr;
   
   /* Set rtc configuration parameters */
   halRTCInit(&s_rtcConf_s);
   
   /* register recieve callback */
   halRTCRegisterCb(&rtcCallback);
   
   /* send registration frame on startup */
   appRadioTxSendReg();
   
//   /* wait a couple of us before sending the first temperature, so the TempRx is ready */
//   for(us_dummyCtr = 0U; us_dummyCtr < 65000U; us_dummyCtr++)
//   {
//      uint16_t a = 0U;
//      a = 50000U;
//      a++;
//		 } // SRO: better to use supported delay functions like nrf_delay_us();...
//   
//   /* send first temperature */
//   appRadioTxSend(halTempGetTemp());
}

/*---------------------------------------------------------------------------------------------+
|  local functions                                                                             |
+---------------------------------------------------------------------------------------------*/
static void rtcCallback(void)
{
	 static int I = 0;
	 if(I == RSSI_AJUST_PACK_NBR)
	 {
		 I = 0;
		 #ifdef DEBUG_INFORMATION
//		 simple_uart_putstring((const uint8_t *)"RSSI_DEMANDE  ");
		 #endif
		 appRadioTxSendRSSI(halTempGetTemp());
	 }
	 else
	 {
		 I++;
		 #ifdef DEBUG_INFORMATION
//		 simple_uart_putstring((const uint8_t *)"NORMAL_PACKET  ");
		 #endif
		 appRadioTxSend(halTempGetTemp());
	 }
}
