/**
 * @file    hal_factory.c
 * @author  Jan Grossrieder
 * @date    November, 2014
 * @brief   see hal_factory.h
 */

/* headers for this module -------------------------------------------------------------------*/
#include "hal_factory.h"
#include "nrf.h"

/* standard headers (ANSI- and Posix standards) ----------------------------------------------*/
#include <stdint.h>

/*---------------------------------------------------------------------------------------------+
|  functions                                                                                   |
+---------------------------------------------------------------------------------------------*/
const uint32_t *halFactoryGetDeviceID(void)
{
   return (const uint32_t *)NRF_FICR->DEVICEID;
}
