/**
 * @file    app_scheduler.c
 * @author  Jan Grossrieder
 * @date    November, 2014
 * @brief   see @ref app_schedRx_desc
 */

/* headers for this module -------------------------------------------------------------------*/
#include "app_scheduler.h"
#include "app_radio_rx.h"
#include "app_dac.h"
#include "hal_rtc.h"
#include "hal_gpio.h"
#ifdef DEBUG_INFORMATION
#include "simple_uart.h"
#endif

/* standard headers --------------------------------------------------------------------------*/
#include <stdint.h>
#include <stdbool.h>
#include <stdio.h>

/*---------------------------------------------------------------------------------------------+
|  local constants and macros                                                                  |
+---------------------------------------------------------------------------------------------*/
#define RTC_STEP_IN_MS  125UL /**< RTC Step in ms, it will be used to calculate the prescaler */
#define RTC_INT_IN_MS   250UL /**< RTC Intervall time in ms, used to calculate the comp value */
#define RTC_PRESCALER   (((32768UL * RTC_STEP_IN_MS) / 1000UL) - 1UL)
#define RTC_COMP_VAL    (RTC_INT_IN_MS / RTC_STEP_IN_MS)

#define GPIO_PIN_BUT0   17UL  /**< Pin number of button 0 (0...31) */
#define GPIO_PIN_BUT1   18UL  /**< Pin number of button 1 (0...31) */
#define GPIO_PIN_LED0   21UL  /**< Pin number of LED 0 (0...31) */
#define GPIO_PIN_LED1   22UL  /**< Pin number of LED 1 (0...31) */

#define RADIO_REG_TIME_IN_MS     10000UL  /**< max. time in ms, the registration can take */
#define RADIO_REG_COMP_VAL       (RADIO_REG_TIME_IN_MS / RTC_INT_IN_MS)

/**
 * if within this time in ms, the radio doesn't recieve a new temperature, the according DAC
 * output fill be forced to high impedance
 */
#define RADIO_TIMEOUT_IN_MS      70000UL
#define RADIO_TIMEOUT_COMP_VAL   (RADIO_TIMEOUT_IN_MS / RTC_INT_IN_MS)

/**
 * scheduler states
 */
typedef enum
{
   SCHED_STATE_WAIT,          /**< state waiting */
   SCHED_STATE_REG_ENABLED0,  /**< state registration tx0 enabled */
   SCHED_STATE_REG_ENABLED1   /**< state registration tx1 enabled */
} e_schedStates_t;

/**
 * LED states
 */
typedef enum
{
   LED_STATE_STATIC,    /**< static value (ON, OFF) */
   LED_STATE_BLINKING   /**< blinking LED */
} e_LEDStates_t;

/*---------------------------------------------------------------------------------------------+
|  local functions prototypes                                                                  |
+---------------------------------------------------------------------------------------------*/
/**
 * Handles the run task in state wait
 *
 * @param e_idx_p the requested DAC output
 */
static void schedulerRunStateWait(e_dacOut_t e_idx_p);

/**
 * Handles the run task in state register
 *
 * @param e_idx_p the requested DAC output
 */
static void schedulerRunStateRegister(e_dacOut_t e_idx_p);

/**
 * This function is called on every RTC compare event.
 */
static void rtcCallback(void);

/**
 * This function is called on a button0 event.
 */
static void gpioCallbackBut0(void);

/**
 * This function is called on a button1 event.
 */
static void gpioCallbackBut1(void);

/*---------------------------------------------------------------------------------------------+
|  module global variables                                                                     |
+---------------------------------------------------------------------------------------------*/
/** RTC configuration */
static const s_rtcConfig_t s_rtcConf_s = {RTC_PRESCALER, true, RTC_COMP_VAL};

/** GPIO configuration of button0 */
static const s_gpioConfig_t s_gpioConfBut0_s = 
   {GPIO_PIN_BUT0, GPIO_PIN_INPUT, GPIO_PIN_LOW, GPIO_PULL_UP};

/** GPIO configuration of button1 */
static const s_gpioConfig_t s_gpioConfBut1_s = 
   {GPIO_PIN_BUT1, GPIO_PIN_INPUT, GPIO_PIN_LOW, GPIO_PULL_UP};

/** GPIO configuration of LED0 */
static const s_gpioConfig_t s_gpioConfLED0_s = 
   {GPIO_PIN_LED0, GPIO_PIN_OUTPUT, GPIO_PIN_LOW, GPIO_PULL_NONE};

/** GPIO configuration of LED1 */
static const s_gpioConfig_t s_gpioConfLED1_s = 
   {GPIO_PIN_LED1, GPIO_PIN_OUTPUT, GPIO_PIN_LOW, GPIO_PULL_NONE};

/** GPIOTE configuration of button0 */
static const s_gpioIntConfig_t s_gpioIntConfBut0_s = 
   {GPIO_PIN_BUT0, GPIO_INT_POL_HITOLO, &gpioCallbackBut0};

/** GPIOTE configuration of button1 */
static const s_gpioIntConfig_t s_gpioIntConfBut1_s = 
   {GPIO_PIN_BUT1, GPIO_INT_POL_HITOLO, &gpioCallbackBut1};

/** global scheduler actual state */
static e_schedStates_t e_schedState_s = SCHED_STATE_WAIT;

/** LED scheduler for LED0 */
static e_LEDStates_t e_ledState0_s = LED_STATE_STATIC;

/** LED scheduler for LED1 */
static e_LEDStates_t e_ledState1_s = LED_STATE_STATIC;

/** LED toggle flag for LED0 */
static uint8_t ub_led0Toggle_s = 0U;

/** LED toggle flag for LED1 */
static uint8_t ub_led1Toggle_s = 0U;

/** radio registration counter */
static uint32_t ul_radioRegCnt_s = 0UL;

/** radio timeout counter */
static uint32_t ul_radioTimeoutCnt_s[DAC_QTY] = {0UL, 0UL};

/** flag if radio is timeout */
static bool isRadioTimeout[DAC_QTY] = {true, true};

/*---------------------------------------------------------------------------------------------+
|  functions                                                                                   |
+---------------------------------------------------------------------------------------------*/
void appSchedulerInit(void)
{
   /* Set rtc configuration parameters */
   halRTCInit(&s_rtcConf_s);
   
   /* register recieve callback */
   halRTCRegisterCb(&rtcCallback);
   
   /* register GPIO */
   halGPIOInitPin(&s_gpioConfBut0_s);
   halGPIOInitPin(&s_gpioConfBut1_s);
   halGPIOInitPin(&s_gpioConfLED0_s);
   halGPIOInitPin(&s_gpioConfLED1_s);
   
   /* register GPIOTE */
   halGPIOInitInterrupt(&s_gpioIntConfBut0_s);
   halGPIOInitInterrupt(&s_gpioIntConfBut1_s);
   
   /* initialize DAC */
   appDACInit();
}

void appSchedulerRun(void)
{
   switch(e_schedState_s)
   {
      case SCHED_STATE_WAIT:
         schedulerRunStateWait(DAC_OUT0);
         schedulerRunStateWait(DAC_OUT1);
         break;
      
      case SCHED_STATE_REG_ENABLED0:
         schedulerRunStateRegister(DAC_OUT0);
         break;
      
      case SCHED_STATE_REG_ENABLED1:
         schedulerRunStateRegister(DAC_OUT1);
         break;
      
      default:
         break;
   }
}

/*---------------------------------------------------------------------------------------------+
|  local functions                                                                             |
+---------------------------------------------------------------------------------------------*/
static void schedulerRunStateWait(e_dacOut_t e_idx_p)
{
   if(e_idx_p < DAC_QTY)
   {
      if(appRadioRxIsNewTempAvailable(e_idx_p) == true)
      {
         #ifdef DEBUG_INFORMATION
         char test[50] = {'\0'};
         (void)sprintf(test, "TempTx%d RSSI: %d temp: %f\r\n", e_idx_p,
						appRadioRxGetRSSI(e_idx_p),
            ((float)appRadioRxGetTemp(e_idx_p))/4.0);
         simple_uart_putstring((const uint8_t *)test);
         #endif
         
         /* send temp to SPI */
         appDACSet(e_idx_p, appRadioRxGetTemp(e_idx_p));
         /* reset timeout counter */
         ul_radioTimeoutCnt_s[e_idx_p] = 0UL;
         isRadioTimeout[e_idx_p] = false;
         /* switch on LED */
         if(e_idx_p == DAC_OUT0)
         {
            halGPIOPinSet(GPIO_PIN_LED0, GPIO_PIN_HIGH);
         }
         else
         {
            halGPIOPinSet(GPIO_PIN_LED1, GPIO_PIN_HIGH);
         }
      }
      else if(ul_radioTimeoutCnt_s[e_idx_p] >= RADIO_TIMEOUT_COMP_VAL)
      {
         if(isRadioTimeout[e_idx_p] == false)
         {
            isRadioTimeout[e_idx_p] = true;
            /* timeout reached, switch off LED and DAC */
            if(e_idx_p == DAC_OUT0)
            {
               halGPIOPinSet(GPIO_PIN_LED0, GPIO_PIN_LOW);
            }
            else
            {
               halGPIOPinSet(GPIO_PIN_LED1, GPIO_PIN_LOW);
            }
            /* set to highest value, to idendicate that the TempTx is lost */
            appDACSetMaxVal(e_idx_p);
         }
      }
   }
}

static void schedulerRunStateRegister(e_dacOut_t e_idx_p)
{
   if(e_idx_p < DAC_QTY)
   {
      if(ul_radioRegCnt_s >= RADIO_REG_COMP_VAL)
      {
         appRadioRxRegisterDeviceStop();
         
         e_schedState_s = SCHED_STATE_WAIT;
         
         if(e_idx_p == DAC_OUT0)
         {
            e_ledState0_s = LED_STATE_STATIC;
            halGPIOPinSet(GPIO_PIN_LED0, 
               ((appRadioIsRegistered(e_idx_p) == true) ? GPIO_PIN_HIGH : GPIO_PIN_LOW));
         }
         else
         {
            e_ledState1_s = LED_STATE_STATIC;
            halGPIOPinSet(GPIO_PIN_LED1, 
               ((appRadioIsRegistered(e_idx_p) == true) ? GPIO_PIN_HIGH : GPIO_PIN_LOW));
         }
         
         /* reset timeout counters */
         ul_radioTimeoutCnt_s[DAC_OUT0] = 0UL;
         ul_radioTimeoutCnt_s[DAC_OUT1] = 0UL;
         
         #ifdef DEBUG_INFORMATION
         simple_uart_putstring((const uint8_t *)"Stop registering device\r\n");
         #endif
      }
      else if(appRadioIsRegistered(e_idx_p) == true)
      {
         if(e_idx_p == DAC_OUT0)
         {
            halGPIOPinSet(GPIO_PIN_LED0, GPIO_PIN_HIGH);
            e_ledState0_s = LED_STATE_STATIC;
         }
         else
         {
            halGPIOPinSet(GPIO_PIN_LED1, GPIO_PIN_HIGH);
            e_ledState1_s = LED_STATE_STATIC;
         }
         /* registration done */
         ul_radioRegCnt_s = RADIO_REG_COMP_VAL;
      }
   }
}

static void rtcCallback(void)
{
   switch(e_ledState0_s)
   {
      case LED_STATE_STATIC:
         break;
      case LED_STATE_BLINKING:
         halGPIOPinSet(GPIO_PIN_LED0, (ub_led0Toggle_s == 0U) ? GPIO_PIN_LOW : GPIO_PIN_HIGH);
         ub_led0Toggle_s ^= 1U;
         break;
      default:
         break;
   }
   
   switch(e_ledState1_s)
   {
      case LED_STATE_STATIC:
         break;
      case LED_STATE_BLINKING:
         halGPIOPinSet(GPIO_PIN_LED1, (ub_led1Toggle_s == 0U) ? GPIO_PIN_LOW : GPIO_PIN_HIGH);
         ub_led1Toggle_s ^= 1U;
         break;
      default:
         break;
   }
   
   if(e_schedState_s > SCHED_STATE_WAIT)
   {
      ul_radioRegCnt_s++;
   }
   else
   {
      ul_radioTimeoutCnt_s[DAC_OUT0]++;
      ul_radioTimeoutCnt_s[DAC_OUT1]++;
   }
}

static void gpioCallbackBut0(void)
{
   if(e_schedState_s == SCHED_STATE_WAIT)
   {
      ul_radioRegCnt_s = 0UL;
      
      /* ready for register */
      e_schedState_s = SCHED_STATE_REG_ENABLED0;
      /* start blinking */
      ub_led0Toggle_s = 0U;
      e_ledState0_s = LED_STATE_BLINKING;
      /* start registering device 0 */
      appRadioRxRegisterDeviceStart(0U);
      
      #ifdef DEBUG_INFORMATION
      simple_uart_putstring((const uint8_t *)"Start registering device 0\r\n");
      #endif
   }
}

static void gpioCallbackBut1(void)
{
   if(e_schedState_s == SCHED_STATE_WAIT)
   {
      ul_radioRegCnt_s = 0UL;
      
      /* ready for register */
      e_schedState_s = SCHED_STATE_REG_ENABLED1;
      /* start blinking */
      ub_led1Toggle_s = 0U;
      e_ledState1_s = LED_STATE_BLINKING;
      /* start registering device 1 */
      appRadioRxRegisterDeviceStart(1U);
      
      #ifdef DEBUG_INFORMATION
      simple_uart_putstring((const uint8_t *)"Start registering device 1\r\n");
      #endif
   }
}
