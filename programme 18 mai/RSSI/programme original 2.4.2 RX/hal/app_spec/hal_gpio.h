/**
 * @file    hal_gpio.h
 * @author  Jan Grossrieder
 * @date    November, 2014
 * @brief   Handles peripherics GPIO and GPIOTE (I/O interrupts)
 */

#ifndef HAL_GPIO_H_INCLUDED
#define HAL_GPIO_H_INCLUDED

/* includes that must be known for this header -----------------------------------------------*/
#include <stdint.h>
#include <stdbool.h>
#include "nrf.h"

/*---------------------------------------------------------------------------------------------+
|  global constants, macros and enums                                                          |
+---------------------------------------------------------------------------------------------*/
/**
 * Pin drection type
 */
typedef enum
{
   GPIO_PIN_INPUT,   /**< pin as input */
   GPIO_PIN_OUTPUT   /**< pin as output */
} e_gpioDir_t;

/**
 * Pin level type
 */
typedef enum
{
   GPIO_PIN_LOW,  /**< pin level high */
   GPIO_PIN_HIGH  /**< pin level low */
} e_gpioLevel_t;

/**
 * Pin pullup type
 */
typedef enum
{
   GPIO_PULL_NONE = GPIO_PIN_CNF_PULL_Disabled, /**< no pullup */
   GPIO_PULL_DOWN = GPIO_PIN_CNF_PULL_Pulldown, /**< pulldown */
   GPIO_PULL_UP = GPIO_PIN_CNF_PULL_Pullup      /**< pullup */
} e_gpioPull_t;

/**
 * Configuration (GPIO) struct
 */
typedef struct
{
   uint8_t ui_pin;            /**< pin number (0...31) */
   e_gpioDir_t e_pinDir;      /**< pin direction */
   e_gpioLevel_t e_pinLevel;  /**< default level after initialisation */
   e_gpioPull_t e_pinPull;    /**< pullup, pulldown on this pin */
} s_gpioConfig_t;

/**
 * Callback type of GPIOTE
 */
typedef void t_gpioCb(void);

/**
 * Interrupt types
 */
typedef enum
{
   GPIO_INT_POL_LOTOHI = GPIOTE_CONFIG_POLARITY_LoToHi,  /**< interrupt on edge low->high */
   GPIO_INT_POL_HITOLO = GPIOTE_CONFIG_POLARITY_HiToLo,  /**< interrupt on edge high->low */
   GPIO_INT_POL_TOGGLE = GPIOTE_CONFIG_POLARITY_Toggle   /**< interrupt on both edges */
} e_gpioIntPol_t;

/**
 * Interrupt configuration (GPIOTE) struct
 */
typedef struct
{
   uint8_t ui_pin;            /**< pin number (0...31) */
   e_gpioIntPol_t e_polarity; /**< Interrupt polarity */
   t_gpioCb *cbFunc;          /**< registered callback function */
} s_gpioIntConfig_t;

/*---------------------------------------------------------------------------------------------+
|  function prototypes                                                                         |
+---------------------------------------------------------------------------------------------*/
/**
 * Initialize the GPIO peripherie.
 *
 * @param p_config_p the configuration
 */
void halGPIOInitPin(const s_gpioConfig_t *const p_config_p);

/**
 * Initialize the GPIOTE peripherie.
 *
 * @param p_config_p the configuration
 */
void halGPIOInitInterrupt(const s_gpioIntConfig_t *const p_config_p);

/**
 * Set the outputlevel.
 *
 * @param ub_pin_p the requested pin (0...31)
 * @param e_level_p output level
 * @note The requested pin must be set as output.
 */
void halGPIOPinSet(uint8_t ub_pin_p, e_gpioLevel_t e_level_p);

/**
 * Get a pins level.
 *
 * @param ub_pin_p the requested pin (0...31)
 * @return the current level
 * @note The requested pin must be set as input.
 */
e_gpioLevel_t halGPIOPinGet(uint8_t ub_pin_p);

#endif
