/**
 * @file    hal_rtc.h
 * @author  Jan Grossrieder
 * @date    November, 2014
 * @brief   Handles the RTC peripheric, configuration and registering callback-function
 */

#ifndef HAL_RTC_H_INCLUDED
#define HAL_RTC_H_INCLUDED

/* includes that must be known for this header -----------------------------------------------*/
#include <stdint.h>
#include <stdbool.h>
#include "nrf.h"

/*---------------------------------------------------------------------------------------------+
|  global constants, macros and enums                                                          |
+---------------------------------------------------------------------------------------------*/
/**
 * RTC configuration struct
 */
typedef struct
{
   uint16_t ui_prescaler;     /**< clock prescaler (12-bit) */
   bool isCompareIntEnabled;  /**< true if interrupt enabled, false if not */
   uint32_t ul_compareVal;    /**< compare interrupt value */
} s_rtcConfig_t;

/**
 * Callback type of rtc interrupt
 */
typedef void t_rtcCompCb(void);

/*---------------------------------------------------------------------------------------------+
|  function prototypes                                                                         |
+---------------------------------------------------------------------------------------------*/
/**
 * Initialize the RTC.
 *
 * @param p_config_p the configuration
 */
void halRTCInit(const s_rtcConfig_t *const p_config_p);

/**
 * Register the callback function which is called if an interrupt request occured
 */
void halRTCRegisterCb(t_rtcCompCb *p_cbFunc_p);

#endif
