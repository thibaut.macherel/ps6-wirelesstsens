/**
 * @file    hal_power.h
 * @author  Jan Grossrieder
 * @date    November, 2014
 * @brief   Handles the power options, such as enable/disable RAM blocks, CPU sleepmode
 */

#ifndef HAL_POWER_H_INCLUDED
#define HAL_POWER_H_INCLUDED

/* includes that must be known for this header -----------------------------------------------*/
#include <stdint.h>
#include <stdbool.h>
#include "nrf.h"

/*---------------------------------------------------------------------------------------------+
|  global constants, macros and enums                                                          |
+---------------------------------------------------------------------------------------------*/
/**
 * RAM blocks
 */
typedef enum
{
   POWER_RAM_8KB,    /**< block0 */
   POWER_RAM_16KB,   /**< block0-1 */
   POWER_RAM_24KB,   /**< block0-2 */
   POWER_RAM_32KB    /**< block0-3 */
} e_powerRam_t;

/**
 * Configuration RAM struct
 */
typedef struct
{
   bool isRamBlock0On;        /**< true if block0 enabled, false if not */
   bool isRamBlock0Retention; /**< true if block0 retention enabled, false if not */
   bool isRamBlock1On;        /**< true if block1 enabled, false if not */
   bool isRamBlock1Retention; /**< true if block1 retention enabled, false if not */
   bool isRamBlock2On;        /**< true if block2 enabled, false if not */
   bool isRamBlock2Retention; /**< true if block2 retention enabled, false if not */
   bool isRamBlock3On;        /**< true if block3 enabled, false if not */
   bool isRamBlock3Retention; /**< true if block3 retention enabled, false if not */
} s_powerConfig_t;

/*---------------------------------------------------------------------------------------------+
|  function prototypes                                                                         |
+---------------------------------------------------------------------------------------------*/
/**
 * Select the active and retentioned RAM blocks.
 *
 * @param s_config_p the RAM configuration
 */
void halPowerRAMSelect(const s_powerConfig_t *const s_config_p);

/**
 * Activate the low power mode with variable latency and set the CPU into
 * sleep mode. A wake up is done at any interrupt requests.
 */
void halPowerEnterSleepMode(void);

#endif
