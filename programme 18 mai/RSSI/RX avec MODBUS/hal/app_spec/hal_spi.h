/**
 * @file    hal_spi.h
 * @author  Jan Grossrieder
 * @date    November, 2014
 * @brief   Handles the SPI peripheric, only Tx
 */

#ifndef HAL_SPI_H_INCLUDED
#define HAL_SPI_H_INCLUDED

/* includes that must be known for this header -----------------------------------------------*/
#include <stdint.h>
#include <stdbool.h>
#include "nrf.h"
#include "hal_gpio.h"

/*---------------------------------------------------------------------------------------------+
|  global constants, macros and enums                                                          |
+---------------------------------------------------------------------------------------------*/
/**
 * SPI clock frequency
 */
typedef enum
{
   SPI_CLK_FREQ_125KBPS = SPI_FREQUENCY_FREQUENCY_K125,  /**< 125kBaud */
   SPI_CLK_FREQ_250KBPS = SPI_FREQUENCY_FREQUENCY_K250,  /**< 250kBaud */
   SPI_CLK_FREQ_500KBPS = SPI_FREQUENCY_FREQUENCY_K500,  /**< 500kBaud */
   SPI_CLK_FREQ_1MBPS = SPI_FREQUENCY_FREQUENCY_M1,      /**< 1MBaud */
   SPI_CLK_FREQ_2MBPS = SPI_FREQUENCY_FREQUENCY_M2,      /**< 2MBaud */
   SPI_CLK_FREQ_4MBPS = SPI_FREQUENCY_FREQUENCY_M4       /**< 4MBaud */
} e_spiFreq_t;

/**
 * Data order
 */
typedef enum
{
   SPI_MSB_FIRST, /**< MSB is first transmitted */
   SPI_LSB_FIRST  /**< LSB is first transmitted */
} e_spiOrder_t;

/**
 * SPI phase
 */
typedef enum
{
   SPI_PHASE_LEADING,   /**< phase is leading */
   SPI_PHASE_TRAILING   /**< phase is trailing */
} e_spiPhase_t;

/**
 * SPI clock polarity
 */
typedef enum
{
   SPI_CLK_POLARITY_HIGH,  /**< as default clock level is high */
   SPI_CLK_POLARITY_LOW    /**< as default clock level is low */
} e_spiPolarity_t;

/**
 * SPI configuration struct
 */
typedef struct
{
   uint8_t ui_pinClk;            /**< clock pin (0...31) */
   uint8_t ui_pinCS;             /**< chip select pin (0...31) */
   uint8_t ui_pinMISO;           /**< MISO pin (0...31) */
   uint8_t ui_pinMOSI;           /**< MOSI pin (0...31) */
   e_spiFreq_t e_freq;           /**< SPI frequency */
   e_spiOrder_t e_order;         /**< data order, MSB or LSB first */
   e_spiPhase_t e_phase;         /**< clock phase */
   e_spiPolarity_t e_polarity;   /**< clock polarity */
} s_spiConfig_t;

/*---------------------------------------------------------------------------------------------+
|  function prototypes                                                                         |
+---------------------------------------------------------------------------------------------*/
/**
 * Initialize the SPI peripherie.
 *
 * @param p_config_p the configuration
 */
void halSPIInit(const s_spiConfig_t *const p_config_p);

/**
 * Start tx session
 *
 * @param ub_data_p the data
 * @param ui_length_p number of bytes to be transmitted
 */
void halSPISendStart(const uint8_t *const ub_data_p, uint16_t ui_length_p);

/**
 * Is SPI busy
 *
 * @return true if busy, false if not
 */
bool halSPIisBusy(void);

#endif
