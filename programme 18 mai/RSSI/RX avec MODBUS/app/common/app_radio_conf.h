/**
 * @file    app_radio_conf.h
 * @author  Jan Grossrieder
 * @date    November, 2014
 * @brief   Radio configuration file
 *
 * This file contains configuration infos for the radio peripheric which are used for both
 * systems TempTx and TempRx. Only the relevant configurations seen from application layer are
 * defined in this file. Lower configurations are defined in @ref hal_radio.h
 */

#ifndef APP_RADIO_CONF_H_INCLUDED
#define APP_RADIO_CONF_H_INCLUDED

/* includes that must be known for this header -----------------------------------------------*/
#include "nrf.h"

/*---------------------------------------------------------------------------------------------+
|  defines for peripherie RADIO                                                                |
+---------------------------------------------------------------------------------------------*/
/* Application layer defines */
#define RADIO_ADDRESS            0UL   /**< Radio adress */
#define RADIO_PAYLOAD_SIZE       11UL  /**< Payload size of one packet */
#define RADIO_TX_MEMBER_NBR      2U    /**< How many tx members are connected */

#define RADIO_PAYLOAD_ID_IDX     1U    /**< Index of the packet where the device ID is stored */
#define RADIO_PAYLOAD_DATA_IDX   9U    /**< Index of the packet where the data is stored */
#define RADIO_PAYLOAD_REG_IDX    0U    /**< Index of the packet where the register Dev is stored */

#define RADIO_NOR_FUNCT_DATA     0xFFU /**< Data sent when in normal funtion (no registration) */
#define RADIO_RSSI_REQUEST			 0xCCU /**< Data sent to request the RSSI value */
#define RADIO_RSSI_VALUE				 0xBBU /**< Data sent when data is RSSI value */
#define RADIO_REGISTRATION_DATA  0xAAU /**< Data sent when in registration */

#endif
