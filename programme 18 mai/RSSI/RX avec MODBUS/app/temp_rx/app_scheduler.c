/**
 * @file    app_scheduler.c
 * @author  Jan Grossrieder
 * @date    November, 2014
 * @brief   see @ref app_schedRx_desc
 */

/* headers for this module -------------------------------------------------------------------*/
#include "app_scheduler.h"
#include "app_radio_rx.h"
#include "app_modbus.h"
#include "hal_rtc.h"
#include "hal_gpio.h"
#ifdef DEBUG_INFORMATION
#include "simple_uart.h"
#endif

/* standard headers --------------------------------------------------------------------------*/
#include <stdint.h>
#include <stdbool.h>
#include <stdio.h>

/*---------------------------------------------------------------------------------------------+
|  local constants and macros                                                                  |
+---------------------------------------------------------------------------------------------*/
#define RTC_STEP_IN_MS  125UL /**< RTC Step in ms, it will be used to calculate the prescaler */
#define RTC_INT_IN_MS   250UL /**< RTC Intervall time in ms, used to calculate the comp value */
#define RTC_PRESCALER   (((32768UL * RTC_STEP_IN_MS) / 1000UL) - 1UL)
#define RTC_COMP_VAL    (RTC_INT_IN_MS / RTC_STEP_IN_MS)

#define GPIO_PIN_BUT1   17UL  /**< Pin number of button 0 (0...31) */
#define GPIO_PIN_BUT2   18UL  /**< Pin number of button 1 (0...31) */
#define GPIO_PIN_LED1   21UL  /**< Pin number of LED 0 (0...31) */
#define GPIO_PIN_LED2   22UL  /**< Pin number of LED 1 (0...31) */

#define RADIO_REG_TIME_IN_MS     10000UL  /**< max. time in ms, the registration can take */
#define RADIO_REG_COMP_VAL       (RADIO_REG_TIME_IN_MS / RTC_INT_IN_MS)

/**
 * if within this time in ms, the radio doesn't recieve a new temperature a register will increment
 */
#define RADIO_TIMEOUT_IN_MS      60000UL
#define RADIO_TIMEOUT_COMP_VAL   (RADIO_TIMEOUT_IN_MS / RTC_INT_IN_MS)

/**
 * scheduler states
 */
typedef enum
{
   SCHED_STATE_WAIT,          /**< state waiting */
   SCHED_STATE_REG_ENABLED0,  /**< state registration tx0 enabled */
   SCHED_STATE_REG_ENABLED1   /**< state registration tx1 enabled */
} e_schedStates_t;

/**
 * LED states
 */
typedef enum
{
   LED_STATE_STATIC,    /**< static value (ON, OFF) */
   LED_STATE_BLINKING   /**< blinking LED */
} e_LEDStates_t;

/*---------------------------------------------------------------------------------------------+
|  local functions prototypes                                                                  |
+---------------------------------------------------------------------------------------------*/
/**
 * Handles the run task in state wait
 *
 * @param e_idx_p the requested Device connected
 */
static void schedulerRunStateWait(e_DevNumb_t e_idx_p);

/**
 * Handles the run task in state register
 *
 * @param e_idx_p the requested Device connected
 */
static void schedulerRunStateRegister(e_DevNumb_t e_idx_p);

/**
 * This function is called on every RTC compare event.
 */
static void rtcCallback(void);

/**
 * This function is called on a button0 event.
 */
static void gpioCallbackBut0(void);

/**
 * This function is called on a button1 event.
 */
static void gpioCallbackBut1(void);

/*---------------------------------------------------------------------------------------------+
|  module global variables                                                                     |
+---------------------------------------------------------------------------------------------*/
/** RTC configuration */
static const s_rtcConfig_t s_rtcConf_s = {RTC_PRESCALER, true, RTC_COMP_VAL};

/** GPIO configuration of button0 */
static const s_gpioConfig_t s_gpioConfBut0_s = 
   {GPIO_PIN_BUT1, GPIO_PIN_INPUT, GPIO_PIN_LOW, GPIO_PULL_UP};

/** GPIO configuration of button1 */
static const s_gpioConfig_t s_gpioConfBut1_s = 
   {GPIO_PIN_BUT2, GPIO_PIN_INPUT, GPIO_PIN_LOW, GPIO_PULL_UP};

/** GPIO configuration of LED0 */
static const s_gpioConfig_t s_gpioConfLED0_s = 
   {GPIO_PIN_LED1, GPIO_PIN_OUTPUT, GPIO_PIN_LOW, GPIO_PULL_NONE};

/** GPIO configuration of LED1 */
static const s_gpioConfig_t s_gpioConfLED1_s = 
   {GPIO_PIN_LED2, GPIO_PIN_OUTPUT, GPIO_PIN_LOW, GPIO_PULL_NONE};

/** GPIOTE configuration of button0 */
static const s_gpioIntConfig_t s_gpioIntConfBut0_s = 
   {GPIO_PIN_BUT1, GPIO_INT_POL_HITOLO, &gpioCallbackBut0};

/** GPIOTE configuration of button1 */
static const s_gpioIntConfig_t s_gpioIntConfBut1_s = 
   {GPIO_PIN_BUT2, GPIO_INT_POL_HITOLO, &gpioCallbackBut1};

/** global scheduler actual state */
static e_schedStates_t e_schedState_s = SCHED_STATE_WAIT;

/** LED scheduler for LED0 */
static e_LEDStates_t e_ledState0_s = LED_STATE_STATIC;

/** LED scheduler for LED1 */
static e_LEDStates_t e_ledState1_s = LED_STATE_STATIC;

/** LED toggle flag for LED0 */
static uint8_t ub_led0Toggle_s = 0U;

/** LED toggle flag for LED1 */
static uint8_t ub_led1Toggle_s = 0U;

/** radio registration counter */
static uint32_t ul_radioRegCnt_s = 0UL;

/** radio timeout counter */
static uint32_t ul_radioTimeoutCnt_s[DEV_QTY] = {0UL, 0UL};

/** flag if radio is timeout */
static bool isRadioTimeout[DEV_QTY] = {true, true};

/** Time in minutes from the last valid Data Received */
static uint16_t ValidDataReceiveTime[DEV_QTY] = {0xFFFF, 0xFFFF};

/** Last valid Data Received */
static uint16_t LastValidDataReceiveTemp[DEV_QTY] = {0, 0};

/*---------------------------------------------------------------------------------------------+
|  functions                                                                                   |
+---------------------------------------------------------------------------------------------*/
void appSchedulerInit(void)
{
   /* Set rtc configuration parameters */
   halRTCInit(&s_rtcConf_s);
   
   /* register recieve callback */
   halRTCRegisterCb(&rtcCallback);
   
   /* register GPIO */
   halGPIOInitPin(&s_gpioConfBut0_s);
   halGPIOInitPin(&s_gpioConfBut1_s);
   halGPIOInitPin(&s_gpioConfLED0_s);
   halGPIOInitPin(&s_gpioConfLED1_s);
   
   /* register GPIOTE */
   halGPIOInitInterrupt(&s_gpioIntConfBut0_s);
   halGPIOInitInterrupt(&s_gpioIntConfBut1_s);
	
	 #ifndef DEBUG_INFORMATION
	 modbus_init();
   #endif
}

void appSchedulerRun(void)
{
   switch(e_schedState_s)
   {
      case SCHED_STATE_WAIT:
         schedulerRunStateWait(DEV_1);
         schedulerRunStateWait(DEV_2);
         break;
      
      case SCHED_STATE_REG_ENABLED0:
         schedulerRunStateRegister(DEV_1);
         break;
      
      case SCHED_STATE_REG_ENABLED1:
         schedulerRunStateRegister(DEV_2);
         break;
      
      default:
         break;
   }
}

uint16_t get_Dev_Temp(e_DevNumb_t e_idx_p)
{
	return LastValidDataReceiveTemp[e_idx_p];
}

uint16_t get_Dev_Time(e_DevNumb_t e_idx_p)
{
	return ValidDataReceiveTime[e_idx_p];
}

/*---------------------------------------------------------------------------------------------+
|  local functions                                                                             |
+---------------------------------------------------------------------------------------------*/
static void schedulerRunStateWait(e_DevNumb_t e_idx_p)
{
   if(e_idx_p < DEV_QTY)
   {
      if(appRadioRxIsNewTempAvailable(e_idx_p) == true)
      {
         #ifdef DEBUG_INFORMATION
         char test[50] = {'\0'};
         (void)sprintf(test, "TempTx%d temp: %f\r\n", e_idx_p, 
            ((float)appRadioRxGetTemp(e_idx_p))/4.0);
         simple_uart_putstring((const uint8_t *)test);
         #endif
         
				 /* met � 0 le temps depuis la derni�re valeur re�u */
				 ValidDataReceiveTime[e_idx_p] = 0;
				 
         /* enregistre la derni�re temp�rature re�u */
				 LastValidDataReceiveTemp[e_idx_p] = (appRadioRxGetTemp(e_idx_p)/4);
				 
         /* reset timeout counter */
         isRadioTimeout[e_idx_p] = false;
         /* switch on LED */
         if(e_idx_p == DEV_1)
         {
            halGPIOPinSet(GPIO_PIN_LED1, GPIO_PIN_HIGH);
         }
         else
         {
            halGPIOPinSet(GPIO_PIN_LED2, GPIO_PIN_HIGH);
         }
      }
      else if(ul_radioTimeoutCnt_s[e_idx_p] >= RADIO_TIMEOUT_COMP_VAL)
      {
				 /* incr�mente le registre de temps lorsque aucune donn�es n'est re�u */
				 if(ValidDataReceiveTime[e_idx_p] != 0xFFFF)
			   {
					 ValidDataReceiveTime[e_idx_p]++;
				 }
				 ul_radioTimeoutCnt_s[e_idx_p] = 0UL;
				
         if(isRadioTimeout[e_idx_p] == false)
         {
            isRadioTimeout[e_idx_p] = true;
            /* timeout reached, switch off LED */
            if(e_idx_p == DEV_1)
            {
               halGPIOPinSet(GPIO_PIN_LED1, GPIO_PIN_LOW);
            }
            else
            {
               halGPIOPinSet(GPIO_PIN_LED2, GPIO_PIN_LOW);
            }
         }
      }
   }
}

static void schedulerRunStateRegister(e_DevNumb_t e_idx_p)
{
   if(e_idx_p < DEV_QTY)
   {
      if(ul_radioRegCnt_s >= RADIO_REG_COMP_VAL)
      {
         appRadioRxRegisterDeviceStop();
         
         e_schedState_s = SCHED_STATE_WAIT;
         
         if(e_idx_p == DEV_1)
         {
            e_ledState0_s = LED_STATE_STATIC;
            halGPIOPinSet(GPIO_PIN_LED1, 
               ((appRadioIsRegistered(e_idx_p) == true) ? GPIO_PIN_HIGH : GPIO_PIN_LOW));
         }
         else
         {
            e_ledState1_s = LED_STATE_STATIC;
            halGPIOPinSet(GPIO_PIN_LED2, 
               ((appRadioIsRegistered(e_idx_p) == true) ? GPIO_PIN_HIGH : GPIO_PIN_LOW));
         }
         
         /* reset timeout counters */
         ul_radioTimeoutCnt_s[DEV_1] = 0UL;
         ul_radioTimeoutCnt_s[DEV_2] = 0UL;
         
         #ifdef DEBUG_INFORMATION
         simple_uart_putstring((const uint8_t *)"Stop registering device\r\n");
         #endif
      }
      else if(appRadioIsRegistered(e_idx_p) == true)
      {
         if(e_idx_p == DEV_1)
         {
            halGPIOPinSet(GPIO_PIN_LED1, GPIO_PIN_HIGH);
            e_ledState0_s = LED_STATE_STATIC;
         }
         else
         {
            halGPIOPinSet(GPIO_PIN_LED2, GPIO_PIN_HIGH);
            e_ledState1_s = LED_STATE_STATIC;
         }
         /* registration done */
         ul_radioRegCnt_s = RADIO_REG_COMP_VAL;
      }
   }
}

static void rtcCallback(void)
{
   switch(e_ledState0_s)
   {
      case LED_STATE_STATIC:
         break;
      case LED_STATE_BLINKING:
         halGPIOPinSet(GPIO_PIN_LED1, (ub_led0Toggle_s == 0U) ? GPIO_PIN_LOW : GPIO_PIN_HIGH);
         ub_led0Toggle_s ^= 1U;
         break;
      default:
         break;
   }
   
   switch(e_ledState1_s)
   {
      case LED_STATE_STATIC:
         break;
      case LED_STATE_BLINKING:
         halGPIOPinSet(GPIO_PIN_LED2, (ub_led1Toggle_s == 0U) ? GPIO_PIN_LOW : GPIO_PIN_HIGH);
         ub_led1Toggle_s ^= 1U;
         break;
      default:
         break;
   }
   
   if(e_schedState_s > SCHED_STATE_WAIT)
   {
      ul_radioRegCnt_s++;
   }
   else
   {
      ul_radioTimeoutCnt_s[DEV_1]++;
      ul_radioTimeoutCnt_s[DEV_2]++;
   }
}

static void gpioCallbackBut0(void)
{
   if(e_schedState_s == SCHED_STATE_WAIT)
   {
      ul_radioRegCnt_s = 0UL;
      
      /* ready for register */
      e_schedState_s = SCHED_STATE_REG_ENABLED0;
      /* start blinking */
      ub_led0Toggle_s = 0U;
      e_ledState0_s = LED_STATE_BLINKING;
      /* start registering device 0 */
      appRadioRxRegisterDeviceStart(0U);
      
      #ifdef DEBUG_INFORMATION
      simple_uart_putstring((const uint8_t *)"Start registering device 1\r\n");
      #endif
   }
}

static void gpioCallbackBut1(void)
{
   if(e_schedState_s == SCHED_STATE_WAIT)
   {
      ul_radioRegCnt_s = 0UL;
      
      /* ready for register */
      e_schedState_s = SCHED_STATE_REG_ENABLED1;
      /* start blinking */
      ub_led1Toggle_s = 0U;
      e_ledState1_s = LED_STATE_BLINKING;
      /* start registering device 1 */
      appRadioRxRegisterDeviceStart(1U);
      
      #ifdef DEBUG_INFORMATION
      simple_uart_putstring((const uint8_t *)"Start registering device 2\r\n");
      #endif
   }
}
