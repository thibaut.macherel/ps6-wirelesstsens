/**
 * @file    hal_rtc.h
 * @author  Jan Grossrieder
 * @date    November, 2014
 * @brief   Handles the RTC peripheric, configuration and registering callback-function
 */

#ifndef APP_MODBUS_H_INCLUDED
#define APP_MODBUS_H_INCLUDED

/* includes that must be known for this header -----------------------------------------------*/
#include <stdint.h>
#include <stdbool.h>
#include "nrf.h"


/*---------------------------------------------------------------------------------------------+
|  function prototypes                                                                         |
+---------------------------------------------------------------------------------------------*/

void modbus_init(void);

static void uartCallback(void);

static void ModbusRespond(void);

#endif
