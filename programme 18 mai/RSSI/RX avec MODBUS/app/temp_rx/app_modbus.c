/**
 * @file    hal_rtc.c
 * @author  Jan Grossrieder
 * @date    November, 2014
 * @brief   see hal_rtc.h
 */

/* headers for this module -------------------------------------------------------------------*/
#include "app_modbus.h"
#include "simple_uart.h"
#include "hal_uart_RS485.h"
#include "nrf.h"
#include "app_scheduler.h"


/* standard headers (ANSI- and Posix standards) ----------------------------------------------*/
#include <stdint.h>
#include <stdbool.h>
#include <stdio.h>
#include <string.h>

/*---------------------------------------------------------------------------------------------+
|  local constants and macros                                                                  |
+---------------------------------------------------------------------------------------------*/
#define Start_Modbus_Frame 			':'					// carract�re de d�but de Trame Modbus
#define End_Modbus_Frame_H			'\r'				// avant dernier caract�re de fin de trame Modbus
#define End_Modbus_Frame_L			'\n'				// dernier caract�re de fin de trame Modbus

#define Funct_Exception					0x01				// code d'exception function
#define Adress_Exception				0x02				// code d'exception adress
#define CRC_Exception						0x08				// code d'exception CRC

#define Slave_Modbus_ID 				0x03				// ID modbus du module

#define Read_Register_Func			0x03				// Fonction de lecture (seul fonction accept�e)

#define Dev1_Temp_adress				1001				// adresse pour lecture de temp�rature de Dev1
#define T1_Time_Form_Mes				1002				// adresse pour lecture du temps [min] depuis derni�re r�ception de Dev1

#define Dev2_Temp_adress				1003				// adresse pour lecture de temp�rature de Dev2
#define T2_Time_Form_Mes				1004				// adresse pour lecture du temps [min] depuis derni�re r�ception de Dev2

#define ASCII_Nbr_Caract_Trame	14					// nombre de caract�re en mode ASCII par trame sans caract�re de start ni de fin

#define Modbus_Slave_Adress_Pos	0						// position du premier char d'adresse
#define Modbus_Func_Pos					2						// position du byte d�finissant la fonction
#define Modbus_Regis_toRead_Pos	4						// position du premier byte d�finissant le registre qui sera lu
#define Modbus_Regis_Nbr_Pos		8						// position du premier byte d�finissant le nombre de registre qui sera lu

#define Error_Tram_Except_Pos		4						// position du code d'exception dans la trame de r�ponse lors d'une erreur
#define Error_Tram_CRC_Pos			6						// position du CRC dans la trame de r�ponse lors d'une erreur


#define TransmitOrReceiveSignal_pin_number 1			// pin pour mettre module RS485 en mode reception ou emmission
#define txd_pin_number 2													// pin TX
#define rxd_pin_number 3													// pin RX

/*---------------------------------------------------------------------------------------------+
|  module global variables                                                                     |
+---------------------------------------------------------------------------------------------*/

// buffer de r�ception ASCII
static uint8_t ModbusBuffer[ASCII_Nbr_Caract_Trame] = {0};

// Adresse du registre qui doit �tre lu
static uint16_t Register_toRead = 0;

// Nombre de registre qui doit �tre lu
static uint16_t Nbr_Register_toRead = 0;

// Flag caract�re de d�but de trame re�u
static bool Recep_Start = false;

// Flag avant dernier caract�re de fin de trame re�u
static bool Stop_Data_moins1 = false;

// Flag les caract�res de fin de trame re�u
static bool Stop_Data_receved = false;

// Flag de time out de transmission, si false => temps d�pass�
static bool Time_On = false;


/*---------------------------------------------------------------------------------------------+
|  functions                                                                                   |
+---------------------------------------------------------------------------------------------*/
void modbus_init()
{
	 RS485_over_uart_init(TransmitOrReceiveSignal_pin_number, txd_pin_number, rxd_pin_number);
	 halUARTRegisterCb(&uartCallback);
	 RS485_over_uart_init_interrupt();
}


/*---------------------------------------------------------------------------------------------+
|  local functions                                                                             |
+---------------------------------------------------------------------------------------------*/
static uint8_t AsciiToHex(uint8_t Char)
{
			// si le caract�re est un chiffre
			if((Char>0x2F) && (Char<0x3A))
			{
				return (Char-0x30);
			}
			// si le caract�re est une lettre majuscule
			else if((Char>0x40) && (Char<0x47))
			{
				return (Char-0x37);
			}
			// si le caract�re est une lettre minuscule
			else if((Char>0x60) && (Char<0x67))
			{
				return (Char-0x57);
			}
			else
			{
				return 0x00;
			}
}

static uint8_t HexToAscii(uint8_t Hex)
{
			// si le code hex est un chiffre
			if(Hex<0x0A)
			{
				return (Hex+0x30);
			}
			// si le caract�re est une lettre
			else if((Hex>0x9) && (Hex<0x10))
			{
				return (Hex+0x37);
			}
			else return 0;
}


static void Modbus_Send_Error()
{
	for(uint8_t I = 0 ; I < (Error_Tram_CRC_Pos+1+1) ; I++)
	{
		ModbusBuffer[I] = HexToAscii(ModbusBuffer[I]);
	}
	char Trame[50] = {'\0'};
	(void)sprintf(Trame, "%c%c%c%c%c%c%c%c%c%c%c",Start_Modbus_Frame,ModbusBuffer[Modbus_Slave_Adress_Pos],ModbusBuffer[Modbus_Slave_Adress_Pos+1]
																							 ,ModbusBuffer[Modbus_Func_Pos],ModbusBuffer[Modbus_Func_Pos+1]
																							 ,ModbusBuffer[Error_Tram_Except_Pos],ModbusBuffer[Error_Tram_Except_Pos+1]
																							 ,ModbusBuffer[Error_Tram_CRC_Pos],ModbusBuffer[Error_Tram_CRC_Pos+1]
																							 ,End_Modbus_Frame_H,End_Modbus_Frame_L);
	
	
  RS485_over_uart_putstring((const uint8_t *)Trame);
}

static void LRC_Calculate_Error_Mode()
{
	uint8_t CRC = 0;
	for(uint8_t I = 0; I < (Error_Tram_Except_Pos+2) ; I+=2)
	{
		CRC += (ModbusBuffer[I]<<4);
		CRC += ModbusBuffer[I+1];
	}
	CRC ^= 0xFF;
	CRC++;
	ModbusBuffer[Error_Tram_CRC_Pos]=((CRC&0xF0)>>4);
	ModbusBuffer[Error_Tram_CRC_Pos+1]=(CRC&0x0F);
}

static void Modbus_Send_LRC_Error()
{
	ModbusBuffer[Modbus_Func_Pos]|=0x8;
	ModbusBuffer[Error_Tram_Except_Pos]=(CRC_Exception & 0xF0)>>4;
	ModbusBuffer[Error_Tram_Except_Pos+1]=(CRC_Exception & 0x0F);
	LRC_Calculate_Error_Mode();
	Modbus_Send_Error();
}

static void Modbus_Send_Funct_Error()
{
	ModbusBuffer[Modbus_Func_Pos]|=0x8;
	ModbusBuffer[Error_Tram_Except_Pos]=(Funct_Exception & 0xF0)>>4;
	ModbusBuffer[Error_Tram_Except_Pos+1]=(Funct_Exception & 0x0F);
	LRC_Calculate_Error_Mode();
	Modbus_Send_Error();
}

static void Modbus_Send_Adress_Error()
{
	ModbusBuffer[Modbus_Func_Pos]|=0x8;
	ModbusBuffer[Error_Tram_Except_Pos]=(Adress_Exception & 0xF0)>>4;
	ModbusBuffer[Error_Tram_Except_Pos+1]=(Adress_Exception & 0x0F);
	LRC_Calculate_Error_Mode();
	Modbus_Send_Error();
}

static void LRC_Calculate_Message_Mode()
{
	uint8_t CRC = 0;
	for(uint8_t I = 0; I < (Modbus_Regis_toRead_Pos+4) ; I+=2)
	{
		CRC += (ModbusBuffer[I]<<4);
		CRC += ModbusBuffer[I+1];
	}
	CRC ^= 0xFF;
	CRC++;
	ModbusBuffer[Modbus_Regis_Nbr_Pos]=((CRC&0xF0)>>4);
	ModbusBuffer[Modbus_Regis_Nbr_Pos+1]=(CRC&0x0F);
}

static void Modbus_Send_Data()
{
	uint16_t Data_To_Send = 0;
	
	switch (Register_toRead)
	{
		case Dev1_Temp_adress:
			Data_To_Send = get_Dev_Temp(DEV_1);
			break;
		
		case T1_Time_Form_Mes:
					Data_To_Send = get_Dev_Time(DEV_1);
					break;
		
		case Dev2_Temp_adress:
			Data_To_Send = get_Dev_Temp(DEV_2);
					break;
		
		case T2_Time_Form_Mes:
					Data_To_Send = get_Dev_Time(DEV_2);
					break;
	}
		
	ModbusBuffer[Modbus_Regis_toRead_Pos] = ((Data_To_Send&0xF000)>>12);
	ModbusBuffer[Modbus_Regis_toRead_Pos+1] = ((Data_To_Send&0x0F00)>>8);
	ModbusBuffer[Modbus_Regis_toRead_Pos+2] = ((Data_To_Send&0x00F0)>>4);
	ModbusBuffer[Modbus_Regis_toRead_Pos+3] = Data_To_Send&0x000F;
	
	LRC_Calculate_Message_Mode();	
	
	for(uint8_t I = 0 ; I < (Modbus_Regis_Nbr_Pos+1+1) ; I++)
	{
		ModbusBuffer[I] = HexToAscii(ModbusBuffer[I]);
	}
	
	char Trame[50] = {'\0'};
	(void)sprintf(Trame, "%c%c%c%c%c%c%c%c%c%c%c%c%c",Start_Modbus_Frame,ModbusBuffer[Modbus_Slave_Adress_Pos],ModbusBuffer[Modbus_Slave_Adress_Pos+1]
																																			,(ModbusBuffer[Modbus_Func_Pos]),ModbusBuffer[Modbus_Func_Pos+1]
																																			,ModbusBuffer[Modbus_Regis_toRead_Pos],ModbusBuffer[Modbus_Regis_toRead_Pos+1]
																																			,ModbusBuffer[Modbus_Regis_toRead_Pos+2],ModbusBuffer[Modbus_Regis_toRead_Pos+3]
																																			,ModbusBuffer[Modbus_Regis_Nbr_Pos],ModbusBuffer[Modbus_Regis_Nbr_Pos+1]
																																			,End_Modbus_Frame_H,End_Modbus_Frame_L);
  RS485_over_uart_putstring((const uint8_t *)Trame);
}

static void Modbus_Send_Reponse()
{
	// contr�le du CRC
	uint8_t LRC = 0;
	for(uint8_t I = 0 ; I < (ASCII_Nbr_Caract_Trame) ; I+=2)
	{
		LRC += (ModbusBuffer[I]<<4);
		LRC += ModbusBuffer[I+1];
	}
	if(LRC == 0)
	{
		uint16_t T = ModbusBuffer[Modbus_Regis_toRead_Pos];
		Register_toRead = (T<<12);
		T = ModbusBuffer[Modbus_Regis_toRead_Pos+1];
		Register_toRead |= (T<<8);
		T = ModbusBuffer[Modbus_Regis_toRead_Pos+2];
		Register_toRead |= (T<<4);
		T = ModbusBuffer[Modbus_Regis_toRead_Pos+3];
		Register_toRead |= T;
		
		T = ModbusBuffer[Modbus_Regis_Nbr_Pos];
		Nbr_Register_toRead = (T<<12);
		T = ModbusBuffer[Modbus_Regis_Nbr_Pos+1];
		Nbr_Register_toRead |= (T<<8);
		T = ModbusBuffer[Modbus_Regis_Nbr_Pos+2];
		Nbr_Register_toRead |= (T<<4);
		T = ModbusBuffer[Modbus_Regis_Nbr_Pos+3];
		Nbr_Register_toRead |= T;
		
		// s'il y a une erreur de fonction
		if (((ModbusBuffer[Modbus_Func_Pos]<<4) | ModbusBuffer[Modbus_Func_Pos+1]) != Read_Register_Func)
		{
			Modbus_Send_Funct_Error();
		}
		// s'il y a une erreur de registre		
		else if ((Register_toRead<Dev1_Temp_adress) || (Register_toRead>T2_Time_Form_Mes))
		{
			Modbus_Send_Adress_Error();
		}
		else if (Nbr_Register_toRead!=1)
		{
			Modbus_Send_Adress_Error();
		}
		else
		{
			Modbus_Send_Data();
		}
	}
	// s'il y a une erreur de CRC
	else
	{
		Modbus_Send_LRC_Error();
	}
}

static void uartCallback(void)
{
	uint8_t Data = NRF_UART0->RXD;
  static uint8_t DataPos = 0xFF;
	
		
	// enregistrement des donn�es 
	if(DataPos < (ASCII_Nbr_Caract_Trame))
	{
		ModbusBuffer[DataPos] = AsciiToHex(Data);
	}
	// incr�mente le compteur de byte
	if(DataPos < 0xFF)
	{
		DataPos++;
	}
	
	if(Data == Start_Modbus_Frame)
	{
		Recep_Start = true;
		Stop_Data_moins1 = false;
		Stop_Data_receved = false;
//		Time_On = true;
		Time_On = true;
// 		Start_interrupt;
		DataPos = 0;
		memset(ModbusBuffer, 0, sizeof(ModbusBuffer));
	}
	
	// est ce que les caract�res de fin ont �t� re�u, si oui Stop_Data_receved = true
	if(Data == End_Modbus_Frame_H)
	{
		Stop_Data_moins1 = true;
	}
	else if((Data == End_Modbus_Frame_L) && (Stop_Data_moins1 == true))
	{
		Stop_Data_receved = true;
	}
	else
	{
		Stop_Data_moins1 = false;
		Stop_Data_receved = false;
	}
	
	// faire ceci que si la trame est pour nous
	if ((DataPos>1) && (ModbusBuffer[Modbus_Slave_Adress_Pos] == ((Slave_Modbus_ID & 0xF0) >> 4)) && (ModbusBuffer[Modbus_Slave_Adress_Pos+1] == (Slave_Modbus_ID & 0x0F)))
	{
		// si la reception a commenc� mais que le temps est �coul�
		if((Recep_Start == true) && (Time_On == false))
		{
			Recep_Start = false;
			// Time OUT;
			Modbus_Send_LRC_Error();
		}
		// si la reception a commenc�, que les valeurs de fin de trame ont �t� re�u mais qu'il n'y a pas le bon nombre de caract�res
		else if((Recep_Start == true) && (Stop_Data_receved == true) && (DataPos != (ASCII_Nbr_Caract_Trame+2)))
		{
			Recep_Start = false;
			// LCR
			Modbus_Send_LRC_Error();
		}
		// si la reception a commenc�, que les valeurs de fin de trame n'ont pas �t� re�u mais qu'il y a le bon nombre de caract�res
		else if((Recep_Start == true) && (Stop_Data_receved == false) && (DataPos == (ASCII_Nbr_Caract_Trame+2)))
		{
			Recep_Start = false;
			// LCR
			Modbus_Send_LRC_Error();
		}
		// si tout est OK alors r�pondre
		else if((Recep_Start == true) && (Stop_Data_receved == true) && (DataPos == (ASCII_Nbr_Caract_Trame+2)))
		{
			Recep_Start = false;
			// R�pondre OK
			Modbus_Send_Reponse();
		}
	}	
}

