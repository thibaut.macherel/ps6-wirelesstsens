/**
 * @file    app_dac.c
 * @author  Jan Grossrieder
 * @date    November, 2014
 * @brief   see app_dac.h
 */

/* headers for this module -------------------------------------------------------------------*/
#include "app_dac.h"
#include "hal_spi.h"
#ifdef DEBUG_INFORMATION
#include "simple_uart.h"
#endif

/* standard headers --------------------------------------------------------------------------*/
#include <stdint.h>
#include <stdbool.h>
#include <stdio.h>

#define DAC_GAIN_SELECT_POS      5U    /**< gain select position of MSB byte */
#define DAC_GAIN_SELECT_1X       1U    /**< Gain = 1x Vref = 2.048V */
#define DAC_GAIN_SELECT_2X       0U    /**< Gain = 2x Vref = 4.096V */
#define DAC_OUTPUT_SELECT_POS    7U    /**< output position of MSB byte */
#define DAC_SHUTDOWN_SELECT_POS  4U    /**< shutdown DAC position of MSB byte */
#define DAC_SHUTDOWN_ENABLE      0U    /**< shutdown DAC enabled */
#define DAC_SHUTDOWN_DISABLE     1U    /**< shutdown DAC disabled */

#define DAC_GAIN  DAC_GAIN_SELECT_2X   /**< DAC gain selection */

#define DAC_VOLTAGE_AT_25        2980L /**< voltage at 25 degrees Celsius in mV */
#define DAC_VOLTAGE_PER_DEGREE   10L   /**< voltage per degree in mV per degree */


/*---------------------------------------------------------------------------------------------+
|  module global variables                                                                     |
+---------------------------------------------------------------------------------------------*/
/**
 * Tx buffer
 */
static uint8_t ub_txData[2U] = {0U};

/**
 * SPI configuration
 */
static const s_spiConfig_t s_spiConf_s = {3U, /* clock is pin P0.03 */
                                          1U, /* CS is pin P0.01 */
                                          7U, /* MISO is pin P0.07 */
                                          5U, /* MOSI is pin P0.05 */
                                          SPI_CLK_FREQ_250KBPS, 
                                          SPI_MSB_FIRST, 
                                          SPI_PHASE_LEADING, 
                                          SPI_CLK_POLARITY_HIGH};

/*---------------------------------------------------------------------------------------------+
|  functions                                                                                   |
+---------------------------------------------------------------------------------------------*/
void appDACInit(void)
{
   /* Set SPI configuration parameters */
   halSPIInit(&s_spiConf_s);
   
   /* set output to highest value */
   appDACSetMaxVal(DAC_OUT0);
   appDACSetMaxVal(DAC_OUT1);
}

void appDACSet(e_dacOut_t e_outIdx_p, int16_t si_val_p)
{
   /* calculate DAC value [mV] */
   int16_t si_dacValue = (int16_t)(((((int32_t)si_val_p - 100L) * DAC_VOLTAGE_PER_DEGREE) / 4L) + 
      DAC_VOLTAGE_AT_25);
   
   #ifdef DEBUG_INFORMATION
   char test[50];
   (void)sprintf(test, "Set DAC%d: %d\r\n", e_outIdx_p, si_dacValue);
   simple_uart_putstring((const uint8_t *)test);
   #endif
   
   ub_txData[0] = (e_outIdx_p << DAC_OUTPUT_SELECT_POS) | (DAC_GAIN << DAC_GAIN_SELECT_POS) |
      (DAC_SHUTDOWN_DISABLE << DAC_SHUTDOWN_SELECT_POS) | ((si_dacValue >> 8U) & 0x0FU);
   
   ub_txData[1] = (si_dacValue & 0xFFU);
   
   /* start transmitting */
   halSPISendStart(ub_txData, 2U);
   
   /* wait until transmitting is finished */
   while(halSPIisBusy() == true);
}

void appDACSetMaxVal(e_dacOut_t e_outIdx_p)
{
   #ifdef DEBUG_INFORMATION
   char test[50];
   (void)sprintf(test, "Set DAC%d set max value\r\n", e_outIdx_p);
   simple_uart_putstring((const uint8_t *)test);
   #endif
   
   ub_txData[0] = (e_outIdx_p << DAC_OUTPUT_SELECT_POS) | (DAC_GAIN << DAC_GAIN_SELECT_POS) |
      (DAC_SHUTDOWN_DISABLE << DAC_SHUTDOWN_SELECT_POS) | 0x0FU;
   
   ub_txData[1] = 0xFFU;
   
   /* start transmitting */
   halSPISendStart(ub_txData, 2U);
   
   /* wait until transmitting is finished */
   while(halSPIisBusy() == true);
}

void appDACSetHighZ(e_dacOut_t e_outIdx_p)
{
   #ifdef DEBUG_INFORMATION
   char test[50];
   (void)sprintf(test, "Set DAC%d high Z\r\n", e_outIdx_p);
   simple_uart_putstring((const uint8_t *)test);
   #endif
   
   /* set output0 to high impendance */
   ub_txData[0] = (e_outIdx_p << DAC_OUTPUT_SELECT_POS) | (DAC_GAIN << DAC_GAIN_SELECT_POS) |
      (DAC_SHUTDOWN_ENABLE << DAC_SHUTDOWN_SELECT_POS);
   ub_txData[1] = 0U;
   /* start transmitting */
   halSPISendStart(ub_txData, 2U);
   /* wait until transmitting is finished */
   while(halSPIisBusy() == true);
}
