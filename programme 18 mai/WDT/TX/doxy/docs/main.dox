/**
 * @mainpage Main Page
 *
 * @section sys_desc System description
 * The system can be described by following figure:
 * @verbatim
 TempTx                                             TempRx
 +--------------------------+                      +------------------------+     +----------------+               +------------+
 | nRF9630                  | -------------------> | nRF51822-EK            | --> | Extensionboard | ------------> | Orion      |
 | Beacon with nRF51822 SoC | Bluetooth Low Energy | Evaluationkit nRF51822 | SPI | with DAC       | Analog static | Controller |
 +--------------------------+                      +------------------------+     +----------------+ signal        +------------+
 @endverbatim
 *
 * The TempTx sends every one minute the measured silicon temperature over Bluetooth Low
 * Energy to the TempRx. Then the TempRx will convert the received temperature to a specific value
 * and communicates this value to the Extensionboard whos DAC will generate the reqeuired voltage.
 * As for the Orioncontroller the "Analog static signal" should stay the same as in the previous
 * version with the wired temperature sensor LM335, this signal must be simulated by the DAC as
 * if there is a LM335 connected.
 *
 * @section soft_struct Software Structure
 * The software is decomposed in a Hardware Abstraction Layer (HAL) and Application Layer.
 * As there are two systems TempTx and TempRx to control, the Application Layer is splitted
 * in one part TempTx and another part TempRx. Those two systems have access to all necessary
 * modules in the common HAL. As some radio configuration (such as address, datapack
 * decomposition etc.) are similar for TempTx and TempRx there is a configuratiom file
 * (@ref app_radio_conf.h) accessible for both. Following graph shows the dependency of all
 * modules:
 *
 * @image html SW_Architektur.png
 *
 * @subsection decomp_hal Decomposition HAL
 * <TABLE>
 *    <TR>
 *       <TH>module name
 *       <TH>description
 *    <TR>
 *       <TD>HAL factory
 *       <TD>Refer to @ref hal_factory.h
 *    <TR>
 *       <TD>HAL power
 *       <TD>Refer to @ref hal_power.h
 *    <TR>
 *       <TD>HAL temp
 *       <TD>Refer to @ref hal_temp.h
 *    <TR>
 *       <TD>HAL clock
 *       <TD>Refer to @ref hal_clock.h
 *    <TR>
 *       <TD>HAL radio
 *       <TD>Refer to @ref hal_radio.h
 *    <TR>
 *       <TD>HAL rtc
 *       <TD>Refer to @ref hal_rtc.h
 *    <TR>
 *       <TD>HAL gpio
 *       <TD>Refer to @ref hal_gpio.h
 *    <TR>
 *       <TD>HAL spi
 *       <TD>Refer to @ref hal_spi.h
 * </TABLE>
 *
 * @subsection decomp_appTx Decomposition Application Layer TempTx
 * <TABLE>
 *    <TR>
 *       <TH>module name
 *       <TH>description
 *    <TR>
 *       <TD>Main module
 *       <TD>Refer to @ref main_tx.c
 *    <TR>
 *       <TD>Scheduler
 *       <TD>Refer to @ref app_schedTx_desc
 *    <TR>
 *       <TD>Radio Tx
 *       <TD>Refer to @ref app_radio_tx.h
 * </TABLE>
 *
 * @subsection decomp_appRx Decomposition Application Layer TempRx
 * <TABLE>
 *    <TR>
 *       <TH>module name
 *       <TH>description
 *    <TR>
 *       <TD>Main module
 *       <TD>Refer to @ref main_rx.c
 *    <TR>
 *       <TD>Scheduler
 *       <TD>Refer to @ref app_schedRx_desc
 *    <TR>
 *       <TD>Radio Rx
 *       <TD>Refer to @ref app_radio_rx.h
 *    <TR>
 *       <TD>DAC
 *       <TD>Refer to @ref app_dac.h
 * </TABLE>
 *
 *
 * @section app_radio_conf Radio Communication
 * @subsection app_radio_conf_overview Radio Overview
 * The Bluetooth Low Energy works on a frequency of 2.4GHz. In this application the TempTx has
 * to send datapacks to the TempRx with the measured silicon temperature. To indicate which
 * TempTx sent the datapack, it will also send its device ID (a 64bit unique address, read out
 * of a chip intern register) whithin the datapack. To register new TempTx to the TempRx, the
 * datapack contains also an 8bit data to idendicate if this is a register frame or a normal
 * communication frame.
 * @subsection app_radio_data Radio Datapack
 * A datapack contains following informations:
 * @li Datapacktype (8bit)
 * @li Device ID (64bit)
 * @li Temperature data (16bit signed)
 *
 * The datapacktype contains either the value for a registration (@ref RADIO_REGISTRATION_DATA)
 * or the value for normal function (@ref RADIO_NOR_FUNCT_DATA). In case of a registration, the
 * TempRx will store the received device ID if the registration-mode of the TempRx is started.
 * In normal function, the TempRx will simply convert the received temperature and set the
 * corresponding DAC output. The device ID contains the unique 64bit device ID. The temperature
 * is sent in a 16bit signed value in a resolution of 0.25 degree Celsius per bit.
 *
 * Such a datapack contains 11 bytes and looks like that:
 * @verbatim
 | byte 0       | byte 1 - 8                        | byte 9 - 10  |
 +-----------------------------------------------------------------+
 | Datapacktype | Device ID                         | Temperature  |
 +-----------------------------------------------------------------+
 | MSB      LSB | MSB                           LSB | MSB      LSB |
   @endverbatim
 */
 