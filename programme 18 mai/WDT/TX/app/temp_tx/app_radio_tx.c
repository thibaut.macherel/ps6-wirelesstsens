/**
 * @file    app_radio_tx.c
 * @author  Jan Grossrieder
 * @date    November, 2014
 * @brief   see app_radio_tx.h
 */

/* headers for this module -------------------------------------------------------------------*/
#include "app_radio_tx.h"
#include "hal_radio.h"
#include "hal_factory.h"
#include "app_radio_conf.h"

/* standard headers --------------------------------------------------------------------------*/
#include <stdint.h>
#include <stdbool.h>
#include <stdio.h>

/*---------------------------------------------------------------------------------------------+
|  module global variables                                                                     |
+---------------------------------------------------------------------------------------------*/
/** Transmit packet buffer */
static uint8_t ub_packet_s[RADIO_PAYLOAD_SIZE] = {0U};

/** radio configuration */
static const s_radioConfig_t s_radioConf_s = {RADIO_TXPOWER_0DBM, RADIO_ADDRESS, RADIO_PAYLOAD_SIZE};

/*---------------------------------------------------------------------------------------------+
|  local functions prototypes                                                                  |
+---------------------------------------------------------------------------------------------*/
/**
 * This function copies an 64-Bit value (e. g. device ID) stored in a 2*32 bit array into a
 * 4*8bit array.
 *
 * @param p_src_p address of source
 * @param p_dest_p address of destination
 */
static void copyDeviceID32To8(const uint32_t *const p_src_p, uint8_t *p_dest_p);

/*---------------------------------------------------------------------------------------------+
|  functions                                                                                   |
+---------------------------------------------------------------------------------------------*/
void appRadioTxInit(void)
{
   /* store the device ID */
   copyDeviceID32To8(halFactoryGetDeviceID(), &ub_packet_s[RADIO_PAYLOAD_ID_IDX]);
   
   /* normally functional data */
   ub_packet_s[RADIO_PAYLOAD_REG_IDX] = RADIO_NOR_FUNCT_DATA;
   
   /* Set radio configuration parameters */
   halRadioInit(&s_radioConf_s, (uint32_t)ub_packet_s);
   
   /* power off radio */
   halRadioPowerOff();
}

void appRadioTxSend(int16_t si_temp_p)
{
   ub_packet_s[RADIO_PAYLOAD_DATA_IDX] = (uint8_t)((si_temp_p >> 8U) & 0xFFU);
   ub_packet_s[RADIO_PAYLOAD_DATA_IDX + 1U] = (uint8_t)(si_temp_p & 0xFFU);
   
   /* power on the radio */
   halRadioPowerOn();
   halRadioInit(&s_radioConf_s, (uint32_t)ub_packet_s);
   
   halRadioSendStart();
   
   /* wait for end */
   halRadioWaitEnd();
   
   /* and power off again */
   halRadioPowerOff();
}

void appRadioTxSendReg(void)
{
   ub_packet_s[RADIO_PAYLOAD_REG_IDX] = RADIO_REGISTRATION_DATA;
   
   /* power on the radio */
   halRadioPowerOn();
   halRadioInit(&s_radioConf_s, (uint32_t)ub_packet_s);
   
   halRadioSendStart();
   
   /* wait for end */
   halRadioWaitEnd();
   
   /* and power off again */
   halRadioPowerOff();
   
   /* restore normal data */
   ub_packet_s[RADIO_PAYLOAD_REG_IDX] = RADIO_NOR_FUNCT_DATA;
}

/*---------------------------------------------------------------------------------------------+
|  local functions                                                                             |
+---------------------------------------------------------------------------------------------*/
static void copyDeviceID32To8(const uint32_t *const p_src_p, uint8_t *p_dest_p)
{
   uint8_t ub_ctr;
   
   for(ub_ctr = 0U; ub_ctr < 2U; ub_ctr++)
   {
      p_dest_p[ub_ctr * 4U] = (p_src_p[ub_ctr] >> 24U) & 0xFFU;
      p_dest_p[(ub_ctr * 4U) + 1U] = (p_src_p[ub_ctr] >> 16U) & 0xFFU;
      p_dest_p[(ub_ctr * 4U) + 2U] = (p_src_p[ub_ctr] >> 8U) & 0xFFU;
      p_dest_p[(ub_ctr * 4U) + 3U] = p_src_p[ub_ctr] & 0xFFU;
   }
}
