/**
 * @file    hal_power.c
 * @author  Jan Grossrieder
 * @date    November, 2014
 * @brief   see hal_power.h
 */

/* headers for this module --------------------------------------------------*/
#include "hal_power.h"
#include "nrf.h"

/* standard headers (ANSI- and Posix standards) -----------------------------*/
#include <stdint.h>
#include <stdbool.h>

/*----------------------------------------------------------------------------+
|  functions                                                                  |
+----------------------------------------------------------------------------*/
void halPowerRAMSelect(const s_powerConfig_t *const s_config_p)
{
   NRF_POWER->RAMON = ((s_config_p->isRamBlock0On << POWER_RAMON_ONRAM0_Pos) |
      (s_config_p->isRamBlock0Retention << POWER_RAMON_OFFRAM0_Pos) |
      (s_config_p->isRamBlock1On << POWER_RAMON_ONRAM1_Pos) |
      (s_config_p->isRamBlock1Retention << POWER_RAMON_OFFRAM1_Pos));
   
   NRF_POWER->RAMONB = ((s_config_p->isRamBlock2On << POWER_RAMON_ONRAM2_Pos) |
      (s_config_p->isRamBlock2Retention << POWER_RAMON_OFFRAM2_Pos) |
      (s_config_p->isRamBlock3On << POWER_RAMON_ONRAM3_Pos) |
      (s_config_p->isRamBlock3Retention << POWER_RAMON_OFFRAM3_Pos));
}

void halPowerEnterSleepMode(void)
{
   /* set low power mode, by cost of variable latency by wake up */
   NRF_POWER->TASKS_LOWPWR = 1UL;
   __WFI();
}

void halPowerEnterSleepModeAllOff(void)
{
   /* set low power mode, by cost of variable latency by wake up */
   NRF_POWER->TASKS_LOWPWR = 1UL;
	 NRF_CLOCK->TASKS_HFCLKSTOP = 1;
	 NRF_POWER->RAMON = 0;
   __WFI();
}
