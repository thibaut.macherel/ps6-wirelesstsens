/**
 * @file    hal_clock.c
 * @author  Jan Grossrieder
 * @date    November, 2014
 * @brief   see hal_clock.h
 */

/* headers for this module -------------------------------------------------------------------*/
#include "hal_clock.h"
#include "nrf.h"

/* standard headers (ANSI- and Posix standards) ----------------------------------------------*/
#include <stdint.h>
#include <stdbool.h>

/*---------------------------------------------------------------------------------------------+
|  functions                                                                                   |
+---------------------------------------------------------------------------------------------*/
void halClockInit(const s_clockConfig_t *const p_config_p)
{
   if(p_config_p->isHFClockEnabled == true)
   {
      /* Start crystal oscillator */
      NRF_CLOCK->EVENTS_HFCLKSTARTED = 0;
      NRF_CLOCK->XTALFREQ = 
         (p_config_p->e_clockHFFreq << CLOCK_XTALFREQ_XTALFREQ_Pos);
      NRF_CLOCK->TASKS_HFCLKSTART = 1;
	
	NVIC_SetPriority(POWER_CLOCK_IRQn, 2);
	NVIC_EnableIRQ(POWER_CLOCK_IRQn);
		 
	NRF_CLOCK->INTENSET = CLOCK_INTENSET_HFCLKSTARTED_Enabled << CLOCK_INTENSET_HFCLKSTARTED_Pos;
   }
	 
	 
   
   if(p_config_p->isLFClockEnabled == true)
   {
      /* Start crystal oscillator */
      NRF_CLOCK->EVENTS_LFCLKSTARTED = 0;
      NRF_CLOCK->LFCLKSRC =
         (CLOCK_LFCLKSRC_SRC_Xtal << CLOCK_LFCLKSRC_SRC_Pos);
      NRF_CLOCK->TASKS_LFCLKSTART = 1;

   }
}

/*---------------------------------------------------------------------------------------------+
|  Interrupts                                                                                  |
+---------------------------------------------------------------------------------------------*/
/**
 * Clock IRQ handler
 */
void POWER_CLOCK_IRQHandler(void)
{
   
   /* clear counter */
   NRF_CLOCK->EVENTS_HFCLKSTARTED = 0;
}
