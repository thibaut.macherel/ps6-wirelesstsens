/**
 * @file    hal_wdt.h
 * @author  Thibaut Macherel
 * @date    Mars, 2015
 * @brief   Initialise la watchdog
 */

#ifndef HAL_WDT_H_INCLUDED
#define HAL_WDT_H_INCLUDED

/* includes that must be known for this header -----------------------------------------------*/
#include <stdint.h>
#include <stdbool.h>
#include "nrf.h"

/*---------------------------------------------------------------------------------------------+
|  global constants, macros and enums                                                          |
+---------------------------------------------------------------------------------------------*/
/**
 * WDT configuration struct
 */
typedef struct
{
   bool isRunningWhileCpuSleep;			/**< true if WDT running while CPU is sleeping, false if not */
	 bool isRunningWhileHaltedDebug;  /**< true if WDT running while CPU is halted by the debugger, false if not */
	 bool isIntEnabledSetRegister;    /**< true if interrupt enabled, false if not */
   uint32_t ul_CRV;    /**< Counter reload value in number of 32kiHz clock cycles */
} s_wdtConfig_t;

/**
 * Callback type of wdt interrupt
 */
//typedef void t_wdtIntCb(void);

/*---------------------------------------------------------------------------------------------+
|  function prototypes                                                                         |
+---------------------------------------------------------------------------------------------*/
/**
 * Initialize the WDT.
 *
 * @param p_config_p the configuration
 */
void halWDTInit(const s_wdtConfig_t *const p_config_p);

/**
 * Register the callback function which is called if an interrupt request occured
 */
//void halWDTRegisterCb(t_wdtIntCb *p_cbFunc_p);

#endif
