/**
 * @file    hal_radio.c
 * @author  Jan Grossrieder
 * @date    November, 2014
 * @brief   see hal_radio.h
 */

/* headers for this module -------------------------------------------------------------------*/
#include "hal_radio.h"
#include "nrf.h"

/* standard headers --------------------------------------------------------------------------*/
#include <stdint.h>
#include <stdbool.h>
#include <stdio.h>

/*---------------------------------------------------------------------------------------------+
|  module global variables                                                                     |
+---------------------------------------------------------------------------------------------*/
/**
 * The registered callback function
 */
static t_radioRegCb *recCallbackFunc_s = NULL;

/*---------------------------------------------------------------------------------------------+
|  functions                                                                                   |
+---------------------------------------------------------------------------------------------*/
void halRadioInit(const s_radioConfig_t *const p_config_p, uint32_t ul_payloadPtr_p)
{
   /* radio configuration */
   NRF_RADIO->TXPOWER   = (p_config_p->e_txPower << RADIO_TXPOWER_TXPOWER_Pos);
   NRF_RADIO->FREQUENCY = RADIO_FREQUENCY;
   NRF_RADIO->MODE      = (RADIO_MODE << RADIO_MODE_MODE_Pos);

   /* radio address configuration */
   NRF_RADIO->PREFIX0     = RADIO_ADD_PREFIX0;   /* Prefix byte of addresses 3 to 0 */
   NRF_RADIO->PREFIX1     = RADIO_ADD_PREFIX1;   /* Prefix byte of addresses 7 to 4 */
   NRF_RADIO->BASE0       = RADIO_ADD_BASE0;     /* Base address for prefix 0 */
   NRF_RADIO->BASE1       = RADIO_ADD_BASE1;     /* Base address for prefix 1-7 */
   NRF_RADIO->TXADDRESS   = p_config_p->ul_adress;  /* Set device address */
   NRF_RADIO->RXADDRESSES = (1UL << p_config_p->ul_adress);  /* Enable device address */

   /* packet0 configuration */
   NRF_RADIO->PCNF0 = (RADIO_PKT0_S1_SIZE << RADIO_PCNF0_S1LEN_Pos) |
      (RADIO_PKT0_S0_SIZE << RADIO_PCNF0_S0LEN_Pos) |
      (RADIO_PKT0_PAYLOAD_SIZE << RADIO_PCNF0_LFLEN_Pos);

   /* packet1 configuration */
   NRF_RADIO->PCNF1 = (RADIO_PKT1_WHITEEN << RADIO_PCNF1_WHITEEN_Pos) |
                    (RADIO_PKT1_ENDIAN << RADIO_PCNF1_ENDIAN_Pos) |
                    (RADIO_PKT1_BASE_ADDRESS_LENGTH << RADIO_PCNF1_BALEN_Pos) |
                    (p_config_p->ub_payloadSize << RADIO_PCNF1_STATLEN_Pos) |
                    (p_config_p->ub_payloadSize << RADIO_PCNF1_MAXLEN_Pos);

   /* CRC configuration */
   NRF_RADIO->CRCCNF = (RADIO_CRC_NBR_BITS << RADIO_CRCCNF_LEN_Pos);
   if ((NRF_RADIO->CRCCNF & RADIO_CRCCNF_LEN_Msk) == (RADIO_CRCCNF_LEN_Two << RADIO_CRCCNF_LEN_Pos))
   {
     NRF_RADIO->CRCINIT = 0xFFFFUL; /* Initial value */     
     NRF_RADIO->CRCPOLY = RADIO_CRC_POLY_2_BIT;
   }
   else if ((NRF_RADIO->CRCCNF & RADIO_CRCCNF_LEN_Msk) == (RADIO_CRCCNF_LEN_One << RADIO_CRCCNF_LEN_Pos))
   {
     NRF_RADIO->CRCINIT = 0xFFUL;   /* Initial value */
     NRF_RADIO->CRCPOLY = RADIO_CRC_POLY_1_BIT;
   }
   
   /* set packet pointer */
   NRF_RADIO->PACKETPTR = ul_payloadPtr_p;
   
   /* configure shortcuts between events (ready -> start, end -> disabled) */
   NRF_RADIO->SHORTS = (RADIO_SHORTS_READY_START_Enabled << RADIO_SHORTS_READY_START_Pos) |
      (RADIO_SHORTS_END_DISABLE_Enabled << RADIO_SHORTS_END_DISABLE_Pos);
}

void halRadioSendStart(void)
{
   /* clear events */
   NRF_RADIO->EVENTS_READY = 0U;
   NRF_RADIO->EVENTS_END = 0U;
   NRF_RADIO->EVENTS_DISABLED = 0U;
   
   /* start tx */
   NRF_RADIO->TASKS_TXEN = 1;
}

void halRadioWaitEnd(void)
{
   while(NRF_RADIO->EVENTS_DISABLED == 0U);
}

void halRadioRecStart(void)
{
   /* set high priority */
   NVIC_SetPriority(RADIO_IRQn, 1);
   NVIC_EnableIRQ(RADIO_IRQn);
   
   /* enable interrupt for end */
   NRF_RADIO->INTENSET |= (RADIO_INTENSET_END_Enabled << RADIO_INTENSET_END_Pos);
   
   /* go direct form disabled to rx enable state */
   NRF_RADIO->SHORTS |= (RADIO_SHORTS_DISABLED_RXEN_Enabled << RADIO_SHORTS_DISABLED_RXEN_Pos);
   
   /* clear events */
   NRF_RADIO->EVENTS_READY = 0U;
   NRF_RADIO->EVENTS_END = 0U;
   NRF_RADIO->EVENTS_DISABLED = 0U;
   
   /* start rx */
   NRF_RADIO->TASKS_RXEN = 1;
}

void halRadioRecStop(void)
{
   /* disable interrupt for end */
   NRF_RADIO->INTENCLR |= (RADIO_INTENCLR_END_Enabled << RADIO_INTENCLR_END_Pos);
   
   /* do not go direct form disabled to rx enable state */
   NRF_RADIO->SHORTS &= (RADIO_SHORTS_DISABLED_RXEN_Disabled << RADIO_SHORTS_DISABLED_RXEN_Pos);
}

void halRadioRecRegisterCb(t_radioRegCb *p_cbFunc_p)
{
   recCallbackFunc_s = p_cbFunc_p;
}

void halRadioPowerOff(void)
{
   NRF_RADIO->POWER = (RADIO_POWER_POWER_Disabled << RADIO_POWER_POWER_Pos);
}

void halRadioPowerOn(void)
{
   NRF_RADIO->POWER = (RADIO_POWER_POWER_Enabled << RADIO_POWER_POWER_Pos);
}

/*---------------------------------------------------------------------------------------------+
|  Interrupts                                                                                  |
+---------------------------------------------------------------------------------------------*/
/**
 * Radio IRQ handler
 */
void RADIO_IRQHandler(void)
{
   if(recCallbackFunc_s != NULL)
   {
      recCallbackFunc_s();
   }
   
   /* clear events */
   NRF_RADIO->EVENTS_READY = 0U;
   NRF_RADIO->EVENTS_END = 0U;
   NRF_RADIO->EVENTS_DISABLED = 0U;
}
