/**
 * @file    app_modbus.c
 * @author  Thibaut Macherel
 * @date    2015
 * @brief   see hal_uart_RS485.h
 */

/* headers for this module -------------------------------------------------------------------*/
#include "app_modbus.h"
#include "simple_uart.h"
#include "hal_uart_RS485.h"
#include "nrf.h"
#include "app_scheduler.h"
#include "hal_rtc.h"


/* standard headers (ANSI- and Posix standards) ----------------------------------------------*/
#include <stdint.h>
#include <stdbool.h>
#include <stdio.h>
#include <string.h>

/*---------------------------------------------------------------------------------------------+
|  local constants and macros                                                                  |
+---------------------------------------------------------------------------------------------*/
#define Start_Modbus_Frame 			':'					// carract�re de d�but de Trame Modbus
#define End_Modbus_Frame_H			'\r'				// avant dernier caract�re de fin de trame Modbus
#define End_Modbus_Frame_L			'\n'				// dernier caract�re de fin de trame Modbus

#define MaxTramCaractNbr				14					// nombre de caract�res maximum dans la trame non inclu caract�re de d�but et fin

#define Funct_Exception					0x01				// code d'exception function
#define Adress_Exception				0x02				// code d'exception adress
#define CRC_Exception						0x08				// code d'exception CRC

#define Slave_Modbus_ID 				0x03				// ID modbus du module

#define Read_Register_Func			0x03				// Fonction de lecture (seul fonction accept�e)

#define Dev1_Temp_adress				1001				// adresse pour lecture de temp�rature de Dev1
#define T1_Time_Form_Mes				1002				// adresse pour lecture du temps [min] depuis derni�re r�ception de Dev1

#define Dev2_Temp_adress				1003				// adresse pour lecture de temp�rature de Dev2
#define T2_Time_Form_Mes				1004				// adresse pour lecture du temps [min] depuis derni�re r�ception de Dev2

#define RTC_PRESCALER						0						// prescaler pour le RTC
#define RTC_TIME_OUT						163					// time out pour la r�ception de la commande via modbus t[s]=(comp_val+1)/32768 => 163 : time out = 5 ms
#define RTC_TIME_OK							32						// temps entre la fin de la r�ception et l'envoie de la r�ponse t[s]=(comp_val+1)/32768 => 2 : r�ponse apr�s ~ 1 ms

#define Modbus_Slave_Adress_Pos	0						// position du premier char d'adresse
#define Modbus_Func_Pos					2						// position du byte d�finissant la fonction
#define Modbus_Regis_toRead_Pos	4						// position du premier byte d�finissant le registre qui sera lu
#define Modbus_Regis_Nbr_Pos		8						// position du premier byte d�finissant le nombre de registre qui sera lu

#define Error_Tram_Except_Pos		4						// position du code d'exception dans la trame de r�ponse lors d'une erreur
#define Error_Tram_CRC_Pos			6						// position du CRC dans la trame de r�ponse lors d'une erreur


#define TransmitOrReceiveSignal_pin_number 1			// pin pour mettre module RS485 en mode reception ou emmission
#define txd_pin_number 2													// pin TX
#define rxd_pin_number 3													// pin RX

/*---------------------------------------------------------------------------------------------+
|  module global variables                                                                     |
+---------------------------------------------------------------------------------------------*/
/** RTC1 configuration */
static const s_rtcConfig_t s_rtcConf_s = {RTC_PRESCALER, true, RTC_TIME_OUT};

// buffer de r�ception
static uint8_t ModbusBuffer[MaxTramCaractNbr] = {0};

// Conteur de caract�res re�u
static uint8_t CaractCounter = 0xFF;

// Etat de la machine d'�tat
static uint8_t StateMachine = 0;

// Registre � lire selon r�ception
static uint16_t RegisterToRead = 0;

static uint16_t RegisterNbrToRead = 0;


/*---------------------------------------------------------------------------------------------+
|  local functions prototypes                                                                  |
+---------------------------------------------------------------------------------------------*/
// Fonction appel� � chaque �v�nement de RTC1
static void rtc1Callback(void);

// Traduit un code ASCII en hexadecimal
static uint8_t AsciiToHex(uint8_t Char);

// Traduit un chiffre ou lettre hexadecimal en ASCII
static uint8_t HexToAscii(uint8_t Hex);

/*---------------------------------------------------------------------------------------------+
|  functions                                                                                   |
+---------------------------------------------------------------------------------------------*/
void modbus_init()
{
	 RS485_over_uart_init(TransmitOrReceiveSignal_pin_number, txd_pin_number, rxd_pin_number);
	 halUARTRegisterCb(&uartCallback);
	 RS485_over_uart_init_interrupt();
	
	 /* Set rtc configuration parameters */
   halRTC1Init(&s_rtcConf_s, false);
   
   /* register recieve callback */
   halRTC1RegisterCb(&rtc1Callback);
}


/*---------------------------------------------------------------------------------------------+
|  local functions                                                                             |
+---------------------------------------------------------------------------------------------*/

static uint8_t AsciiToHex(uint8_t Char)
{
			// si le caract�re est un chiffre
			if((Char>0x2F) && (Char<0x3A))
			{
				return (Char-0x30);
			}
			// si le caract�re est une lettre majuscule
			else if((Char>0x40) && (Char<0x47))
			{
				return (Char-0x37);
			}
			// si le caract�re est une lettre minuscule
			else if((Char>0x60) && (Char<0x67))
			{
				return (Char-0x57);
			}
			else
			{
				return 0x00;
			}
}

static uint8_t HexToAscii(uint8_t Hex)
{
			// si le code hex est un chiffre
			if(Hex<0x0A)
			{
				return (Hex+0x30);
			}
			// si le caract�re est une lettre
			else if((Hex>0x9) && (Hex<0x10))
			{
				return (Hex+0x37);
			}
			else return 0;
}

static void SendData()
{
	uint16_t Data_To_Send = 0;
	
	switch (RegisterToRead)
	{
		case Dev1_Temp_adress:
			Data_To_Send = get_Dev_Temp(DEV_1);
			break;
		
		case T1_Time_Form_Mes:
					Data_To_Send = get_Dev_Time(DEV_1);
					break;
		
		case Dev2_Temp_adress:
			Data_To_Send = get_Dev_Temp(DEV_2);
					break;
		
		case T2_Time_Form_Mes:
					Data_To_Send = get_Dev_Time(DEV_2);
					break;
	}
	
	ModbusBuffer[Modbus_Regis_toRead_Pos] = ((Data_To_Send&0xF000)>>12);
	ModbusBuffer[Modbus_Regis_toRead_Pos+1] = ((Data_To_Send&0x0F00)>>8);
	ModbusBuffer[Modbus_Regis_toRead_Pos+2] = ((Data_To_Send&0x00F0)>>4);
	ModbusBuffer[Modbus_Regis_toRead_Pos+3] = Data_To_Send&0x000F;
	
	uint8_t LRC = 0;
	for(uint8_t I = 0; I < (Modbus_Regis_Nbr_Pos) ; I+=2)
	{
		LRC += (ModbusBuffer[I]<<4);
		LRC += ModbusBuffer[I+1];
	}
	LRC ^= 0xFF;
	LRC++;
	ModbusBuffer[Modbus_Regis_Nbr_Pos]=((LRC&0xF0)>>4);
	ModbusBuffer[Modbus_Regis_Nbr_Pos+1]=(LRC&0x0F);
	
	// traduit les code hexa en code ASCII
	for(uint8_t I = 0 ; I < (Modbus_Regis_Nbr_Pos+2) ; I++)
	{
		ModbusBuffer[I] = HexToAscii(ModbusBuffer[I]);
	}
	
	char Trame[50] = {'\0'};
	(void)sprintf(Trame, "%c%c%c%c%c%c%c%c%c%c%c%c%c",Start_Modbus_Frame,ModbusBuffer[Modbus_Slave_Adress_Pos],ModbusBuffer[Modbus_Slave_Adress_Pos+1]
																																			,(ModbusBuffer[Modbus_Func_Pos]),ModbusBuffer[Modbus_Func_Pos+1]
																																			,ModbusBuffer[Modbus_Regis_toRead_Pos],ModbusBuffer[Modbus_Regis_toRead_Pos+1]
																																			,ModbusBuffer[Modbus_Regis_toRead_Pos+2],ModbusBuffer[Modbus_Regis_toRead_Pos+3]
																																			,ModbusBuffer[Modbus_Regis_Nbr_Pos],ModbusBuffer[Modbus_Regis_Nbr_Pos+1]
																																			,End_Modbus_Frame_H,End_Modbus_Frame_L);
  RS485_over_uart_putstring((const uint8_t *)Trame);
	
}





static void SendError(uint8_t Error)
{
	// met le code d'erreur dans le buffer
	ModbusBuffer[Error_Tram_Except_Pos]=(Error & 0xF0)>>4;
	ModbusBuffer[Error_Tram_Except_Pos+1]=(Error & 0x0F);
	ModbusBuffer[Modbus_Func_Pos]|=0x08;
	
	// calcule le LRC
	uint8_t LRC = 0;
	for(uint8_t I = 0; I < (Error_Tram_Except_Pos+2) ; I+=2)
	{
		LRC += (ModbusBuffer[I]<<4);
		LRC += ModbusBuffer[I+1];
	}
	LRC ^= 0xFF;
	LRC++;
	ModbusBuffer[Error_Tram_CRC_Pos]=((LRC&0xF0)>>4);
	ModbusBuffer[Error_Tram_CRC_Pos+1]=(LRC&0x0F);
	
	// traduit les code hexa en code ASCII
	for(uint8_t I = 0 ; I < (Error_Tram_CRC_Pos+2) ; I++)
	{
		ModbusBuffer[I] = HexToAscii(ModbusBuffer[I]);
	}
	
	char Trame[50] = {'\0'};
	(void)sprintf(Trame, "%c%c%c%c%c%c%c%c%c%c%c",Start_Modbus_Frame,ModbusBuffer[Modbus_Slave_Adress_Pos],ModbusBuffer[Modbus_Slave_Adress_Pos+1]
																							 ,ModbusBuffer[Modbus_Func_Pos],ModbusBuffer[Modbus_Func_Pos+1]
																							 ,ModbusBuffer[Error_Tram_Except_Pos],ModbusBuffer[Error_Tram_Except_Pos+1]
																							 ,ModbusBuffer[Error_Tram_CRC_Pos],ModbusBuffer[Error_Tram_CRC_Pos+1]
																							 ,End_Modbus_Frame_H,End_Modbus_Frame_L);
	
	RS485_over_uart_putstring((const uint8_t *)Trame);
}


static void uartCallback(void)
{
	uint8_t Data = NRF_UART0->RXD;
	
	switch (StateMachine)
	{
		case 0:
			if(Data == Start_Modbus_Frame)
			{
				memset(ModbusBuffer, 0, MaxTramCaractNbr); // remet le tableau � z�ro
				StateMachine = 1;
				CaractCounter = 0;
				halRTC1ChangeCompVal(RTC_TIME_OUT); // d�marre le compteur de Time out
			}
			break;
			
		case 1:
			if(Data == End_Modbus_Frame_H)
			{
				StateMachine = 2;
			}
			else
			{
				if(CaractCounter < MaxTramCaractNbr ) ModbusBuffer[CaractCounter] = AsciiToHex(Data); // enregistre la donn�e re�u
				if(CaractCounter < 0xFF) CaractCounter++;
			}
			break;
			
		case 2:
			if(Data == End_Modbus_Frame_L)
			{
				StateMachine = 3;	// Forme du message correcte
				halRTC1ChangeCompVal(RTC_TIME_OK); // r�ponds dans un petit moment
			}
			else
			{
				StateMachine = 0xff; // Caract�re de fin non re�u
			}
			break;
	}  
}

static void rtc1Callback(void)
{
	halRTC1Stop(); // stop le RTC
	
	// Ne traiter que si la commande concerne notre adresse
	if(Slave_Modbus_ID == (ModbusBuffer[Modbus_Slave_Adress_Pos]<<4 | ModbusBuffer[Modbus_Slave_Adress_Pos+1]))
	{
		if(StateMachine == 3) // Si la forme du message est OK
		{
			// contr�le du LRC
			uint8_t LRC = 0;
			for( uint8_t I = 0 ; I < MaxTramCaractNbr ; I+=2)
			{
				LRC += ModbusBuffer[I]<<4;
				LRC += ModbusBuffer[I+1];
			}
			if((LRC == 0) && (CaractCounter<(MaxTramCaractNbr+1)))
			{
				if(Read_Register_Func == (ModbusBuffer[Modbus_Func_Pos]<<4 | ModbusBuffer[Modbus_Func_Pos+1]))
				{
					RegisterToRead = (ModbusBuffer[Modbus_Regis_toRead_Pos]<<12) + (ModbusBuffer[Modbus_Regis_toRead_Pos+1]<<8)
																		+ (ModbusBuffer[Modbus_Regis_toRead_Pos+2]<<4) + ModbusBuffer[Modbus_Regis_toRead_Pos+3];
					if((RegisterToRead>=Dev1_Temp_adress) || RegisterToRead<=T2_Time_Form_Mes)
					{
						RegisterNbrToRead = (ModbusBuffer[Modbus_Regis_Nbr_Pos]<<12) + (ModbusBuffer[Modbus_Regis_Nbr_Pos+1]<<8)
																				 + (ModbusBuffer[Modbus_Regis_Nbr_Pos+2]<<4) + ModbusBuffer[Modbus_Regis_Nbr_Pos+3];
						if(RegisterNbrToRead == 1)
						{
							// Message OK
							SendData();
						}
						else
						{
							// Registre ERROR
							SendError(Adress_Exception);							
						}
					}
					else
					{
						// Registre ERROR
						SendError(Adress_Exception);
					}
				}
				else
				{
					// Function ERROR
					SendError(Funct_Exception);
				}
			}
			else
			{
				// CRC ERROR
				SendError(CRC_Exception);
			}
		}
		else	// Autrement time out
		{
			SendError(CRC_Exception);
		}
	}
	StateMachine = 0; // remet � l'�tat d'origine
}

