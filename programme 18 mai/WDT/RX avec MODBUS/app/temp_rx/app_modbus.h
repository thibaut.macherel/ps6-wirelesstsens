/**
 * @file    app_modbus.h
 * @author  Jan Grossrieder
 * @date    November, 2014
 * @brief   Handles the RTC peripheric, configuration and registering callback-function
 */

#ifndef APP_MODBUS_H_INCLUDED
#define APP_MODBUS_H_INCLUDED

/* includes that must be known for this header -----------------------------------------------*/
#include <stdint.h>
#include <stdbool.h>
#include "nrf.h"

/**
 * Callback type uart interrupt
 */
 static void uartCallback(void);
 
/*---------------------------------------------------------------------------------------------+
|  function prototypes                                                                         |
+---------------------------------------------------------------------------------------------*/

// initialise le modbus pour répondre au paquets de réception
void modbus_init(void);

#endif
