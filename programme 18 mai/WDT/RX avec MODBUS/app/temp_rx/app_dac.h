/**
 * @file    app_dac.h
 * @author  Jan Grossrieder
 * @date    November, 2014
 * @brief   Handels the DAC
 *
 * The modules main tasks is to approach the DAC module of the extensionboard by SPI. It also
 * converts the temperature to the requested outputvoltage of the DAC. 
 */

#ifndef APP_DAC_H_INCLUDED
#define APP_DAC_H_INCLUDED

/* includes that must be known for this header -----------------------------------------------*/
#include <stdint.h>
#include <stdbool.h>

/*---------------------------------------------------------------------------------------------+
|  global constants, macros and enums                                                          |
+---------------------------------------------------------------------------------------------*/
/**
 * DAC output
 */
typedef enum
{
   DAC_OUT0,   /**< DAC output 0 */
   DAC_OUT1,   /**< DAC output 1 */
   DAC_QTY     /**< Number of DAC outputs */
} e_dacOut_t;

/*---------------------------------------------------------------------------------------------+
|  functions prototypes                                                                        |
+---------------------------------------------------------------------------------------------*/
/**
 * Initialize the DAC and the SPI module.
 */
void appDACInit(void);

/**
 * Set an output of the DAC according to the required temperature. The temperature unit is 0.25
 * degrees Celsius.
 *
 * @param e_outIdx_p Output on which the value should be given
 * @param ui_val_p Value in 0.25 degrees Celsius (12bit)
 * @note This function is blocking. It will unblock if the tx session has finished.
 */
void appDACSet(e_dacOut_t e_outIdx_p, int16_t si_val_p);

/**
 * Set an output of the DAC to the maximal value possible.
 *
 * @param e_outIdx_p Output on which the value should be given
 * @note This function is blocking. It will unblock if the tx session has finished.
 */
void appDACSetMaxVal(e_dacOut_t e_outIdx_p);

/**
 * Set an output of the DAC to high impedance.
 *
 * @param e_outIdx_p Output which should be high impedance
 * @note This function is blocking. It will unblock if the tx session has finished.
 */
void appDACSetHighZ(e_dacOut_t e_outIdx_p);

#endif
