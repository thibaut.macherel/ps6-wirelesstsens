/**
 * @file    app_radio_rx.c
 * @author  Jan Grossrieder
 * @date    November, 2014
 * @brief   see app_radio_rx.h
 */

/* headers for this module -------------------------------------------------------------------*/
#include "app_radio_rx.h"
#include "hal_radio.h"
#include "app_radio_conf.h"
#ifdef DEBUG_INFORMATION
#include "simple_uart.h"
#endif

/* standard headers --------------------------------------------------------------------------*/
#include <stdint.h>
#include <stdbool.h>
#include <stdio.h>

/*---------------------------------------------------------------------------------------------+
|  module global variables                                                                     |
+---------------------------------------------------------------------------------------------*/
/** Received packet buffer */
static uint8_t volatile ub_packet_s[RADIO_PAYLOAD_SIZE] = {0U};

/** radio configuration */
static const s_radioConfig_t s_radioConf_s = {RADIO_TXPOWER_0DBM, RADIO_ADDRESS, RADIO_PAYLOAD_SIZE};

/** registered device ID of tx members */
static uint8_t ub_deviceID_s[RADIO_TX_MEMBER_NBR][8U] = {0U};

/** current tx member to be registered */
static uint8_t ub_curRegTxDevice_s = 0U;

/** received temperature of tx members in 0.25 Celsius degree */
static int16_t si_temp_s[RADIO_TX_MEMBER_NBR] = {0U};

/** flag if device should listen to register tx-devices */
static bool isRegisterDevice_s = false;

/** flag if new temp is available */
static bool isNewTempAvailable_s[RADIO_TX_MEMBER_NBR] = {false, false};

/*---------------------------------------------------------------------------------------------+
|  local functions prototypes                                                                  |
+---------------------------------------------------------------------------------------------*/
/**
 * Callback function which is called by receiving new data.
 */
static void recCallback(void);

/**
 * Simply check if two ID's are equal. Lenght of ID is 4 bytes.
 *
 * @param p_val1_p address of first ID
 * @param p_val2_p address of second ID
 * @return true if equal, false if not
 */
static bool isDeviceIDEqual(const uint8_t *p_val1_p, const uint8_t *p_val2_p);

/**
 * Simple copy of an ID to another address. Length of ID is 4 bytes.
 *
 * @param p_src_p address of source ID
 * @param p_dest_p address of destination ID
 */
static void copyDeviceID(const uint8_t *p_src_p, uint8_t *p_dest_p);

/**
 * Simply clear the ID stored in @ref ub_deviceID_s
 *
 * @param ub_txDev_p the requested tx member
 */
static void clearDeviceID(uint8_t ub_txDev_p);

/*---------------------------------------------------------------------------------------------+
|  functions                                                                                   |
+---------------------------------------------------------------------------------------------*/
void appRadioRxInit(void)
{
   /* Set radio configuration parameters */
   halRadioInit(&s_radioConf_s, (uint32_t)ub_packet_s);
   
   /* register recieve callback */
   halRadioRecRegisterCb(&recCallback);
}

void appRadioRxRecStart(void)
{
   halRadioRecStart();
}

void appRadioRxRecStop(void)
{
   halRadioRecStop();
}

bool appRadioRxIsNewTempAvailable(uint8_t ub_txDev_p)
{
   if(ub_txDev_p < RADIO_TX_MEMBER_NBR)
   {
      return isNewTempAvailable_s[ub_txDev_p];
   }
   
   return false;
}

int16_t appRadioRxGetTemp(uint8_t ub_txDev_p)
{
   if(ub_txDev_p < RADIO_TX_MEMBER_NBR)
   {
      isNewTempAvailable_s[ub_txDev_p] = false;
      return si_temp_s[ub_txDev_p];
   }
   
   return false;
}

void appRadioRxRegisterDeviceStart(uint8_t ub_txDev_p)
{
   if(ub_txDev_p < RADIO_TX_MEMBER_NBR)
   {
      ub_curRegTxDevice_s = ub_txDev_p;
      clearDeviceID(ub_txDev_p);
      isRegisterDevice_s = true;
   }
}

void appRadioRxRegisterDeviceStop(void)
{
   isRegisterDevice_s = false;
}

bool appRadioIsRegistered(uint8_t ub_decIdx_p)
{
   bool isReg = false;
   
   /* check if ID is registered */
   if(ub_decIdx_p < RADIO_TX_MEMBER_NBR)
   {
      uint8_t ub_ctr;
      for(ub_ctr = 0U; ub_ctr < 8U; ub_ctr++)
      {
         if(ub_deviceID_s[ub_decIdx_p][ub_ctr] != 0U)
         {
            isReg = true;
            break;
         }
      }
   }
   
   return isReg;
}


/*---------------------------------------------------------------------------------------------+
|  local functions                                                                             |
+---------------------------------------------------------------------------------------------*/
static void recCallback(void)
{
	if(NRF_RADIO->CRCSTATUS == 1)
	{
   uint8_t ub_ctr;
   
   if(isRegisterDevice_s == true)
   {
      /* registration of new device */
      /* packet has value that allows a registration */
//      if(ub_packet_s[RADIO_PAYLOAD_REG_IDX] == RADIO_REGISTRATION_DATA)
//      {
         #ifdef DEBUG_INFORMATION
         char test[50] = {'\0'};
         uint8_t ub_ctrTest;
         (void)sprintf(test, "Registered TempTx%d: 0x", ub_curRegTxDevice_s);
         simple_uart_putstring((const uint8_t *)test);
         for(ub_ctrTest = 0U; ub_ctrTest < 8U; ub_ctrTest++)
         {
            (void)sprintf(test, "%2x", ub_packet_s[RADIO_PAYLOAD_ID_IDX + ub_ctrTest]);
            simple_uart_putstring((const uint8_t *)test);
         }
         simple_uart_putstring((const uint8_t *)"\r\n");
         #endif
         
         copyDeviceID((const uint8_t *)&ub_packet_s[RADIO_PAYLOAD_ID_IDX], 
            &ub_deviceID_s[ub_curRegTxDevice_s][0U]);
 //     }
   }
   else //if(ub_packet_s[RADIO_PAYLOAD_REG_IDX] == RADIO_NOR_FUNCT_DATA)
   {
      /* normal function, get temperature */
      /* compare device ID */
      for(ub_ctr = 0U; ub_ctr < RADIO_TX_MEMBER_NBR; ub_ctr++)
      {
         if(isDeviceIDEqual((const uint8_t *)&ub_packet_s[RADIO_PAYLOAD_ID_IDX], 
            &ub_deviceID_s[ub_ctr][0U]) == true)
         {
            /* set new temp */
            si_temp_s[ub_ctr] = ((int16_t)ub_packet_s[RADIO_PAYLOAD_DATA_IDX] << 8U) | 
               (int16_t)ub_packet_s[RADIO_PAYLOAD_DATA_IDX + 1U];
            
            /* set temp available */
            isNewTempAvailable_s[ub_ctr] = true;
         }
      }
   }
 }
}

static bool isDeviceIDEqual(const uint8_t *p_val1_p, const uint8_t *p_val2_p)
{
   uint8_t ub_ctr;
   bool isEqual = true;
   
   for(ub_ctr = 0U; ub_ctr < 8U; ub_ctr++)
   {
      if(p_val1_p[ub_ctr] != p_val2_p[ub_ctr])
      {
         isEqual = false;
      }
   }
   
   return isEqual;
}

static void copyDeviceID(const uint8_t *p_src_p, uint8_t *p_dest_p)
{
   uint8_t ub_ctr;
   
   for(ub_ctr = 0U; ub_ctr < 8U; ub_ctr++)
   {
      p_dest_p[ub_ctr] = p_src_p[ub_ctr];
   }
}

static void clearDeviceID(uint8_t ub_txDev_p)
{
   if(ub_txDev_p < RADIO_TX_MEMBER_NBR)
   {
      uint8_t ub_ctr;
      
      for(ub_ctr = 0U; ub_ctr < 8U; ub_ctr++)
      {
         ub_deviceID_s[ub_txDev_p][ub_ctr] = 0U;
      }
   }
}
