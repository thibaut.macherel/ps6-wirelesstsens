/**
 * @file    hal_uart_RS485.h
 * @author  Thibaut Macherel
 * @date    Mars, 2015
 * @brief   Initialise la watchdog
 */

#ifndef HAL_UART_RS485_H_INCLUDED
#define HAL_UART_RS485_H_INCLUDED

/* includes that must be known for this header -----------------------------------------------*/
#include <stdint.h>
#include <stdbool.h>
#include "nrf.h"

/*---------------------------------------------------------------------------------------------+
|  global constants, macros and enums                                                          |
+---------------------------------------------------------------------------------------------*/
/**
 * Callback type of uart interrupt
 */
typedef void t_uartInputDataCb(void);

// initialise la transmission modbus avec un parit� even et un baudrate de 38400
void RS485_over_uart_init(uint8_t TransmitOrReceiveSignal_pin_number, uint8_t txd_pin_number, uint8_t rxd_pin_number);

// active l'interruption lors de la r�ception de caract�res
void RS485_over_uart_init_interrupt(void);

// permet d'envoyer un string
void RS485_over_uart_putstring(const uint8_t * str);

// permet d'enregistrer un callback
void halUARTRegisterCb(t_uartInputDataCb *p_cbFunc_p);

#endif
