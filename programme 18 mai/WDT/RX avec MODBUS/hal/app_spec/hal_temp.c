/**
 * @file    hal_temp.c
 * @author  Jan Grossrieder
 * @date    November, 2014
 * @brief   see hal_temp.h
 */

/* headers for this module -------------------------------------------------------------------*/
#include "hal_temp.h"
#include "nrf.h"

/* standard headers (ANSI- and Posix standards) ----------------------------------------------*/
#include <stdint.h>

/*---------------------------------------------------------------------------------------------+
|  functions                                                                                   |
+---------------------------------------------------------------------------------------------*/
int16_t halTempGetTemp(void)
{
   NRF_TEMP->TASKS_START = 1;
   while(NRF_TEMP->EVENTS_DATARDY == 0);
   
   return (int16_t)NRF_TEMP->TEMP;
}
