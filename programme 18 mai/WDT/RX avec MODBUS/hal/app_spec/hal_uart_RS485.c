/**
 * @file    hal_uart_RS485.h
 * @author  Thibaut Macherel
 * @date    2015
 * @brief   see hal_uart_RS485.h
 */

/* headers for this module -------------------------------------------------------------------*/
#include "hal_uart_RS485.h"
#include "simple_uart.h"
#include "nrf.h"
#include "nrf_gpio.h"

/* standard headers (ANSI- and Posix standards) ----------------------------------------------*/
#include <stdint.h>
#include <stdbool.h>
#include <stdio.h>

/*---------------------------------------------------------------------------------------------+
|  module global variables                                                                     |
+---------------------------------------------------------------------------------------------*/
static uint8_t TransmitOrReceiveSignal_pin = 0;

/**
 * The registered callback function
 */
static t_uartInputDataCb *uartCallbackFunc_s = NULL;

/*---------------------------------------------------------------------------------------------+
|  functions                                                                                   |
+---------------------------------------------------------------------------------------------*/
void RS485_over_uart_init(uint8_t TransmitOrReceiveSignal_pin_number,
													uint8_t txd_pin_number,
													uint8_t rxd_pin_number)
{
		TransmitOrReceiveSignal_pin = TransmitOrReceiveSignal_pin_number;
	
/** @snippet [Configure UART RX and TX pin] */
		nrf_gpio_cfg_output(TransmitOrReceiveSignal_pin_number);
		NRF_GPIO->OUT &= ~(1<<TransmitOrReceiveSignal_pin); // mode passif/�coute : sortie TransmitOrReceive = 0
	  
    nrf_gpio_cfg_output(txd_pin_number);
    nrf_gpio_cfg_input(rxd_pin_number, NRF_GPIO_PIN_NOPULL);

	  // pin CTS et RTS non utilis�e
    NRF_UART0->PSELTXD = txd_pin_number;
    NRF_UART0->PSELRXD = rxd_pin_number;

		NRF_UART0->CONFIG				 = UART_CONFIG_PARITY_Included << UART_CONFIG_PARITY_Pos; // parity even
    NRF_UART0->BAUDRATE      = (UART_BAUDRATE_BAUDRATE_Baud38400 << UART_BAUDRATE_BAUDRATE_Pos);
    NRF_UART0->ENABLE        = (UART_ENABLE_ENABLE_Enabled << UART_ENABLE_ENABLE_Pos);
    NRF_UART0->TASKS_STARTTX = 1;
    NRF_UART0->TASKS_STARTRX = 1;
}

void RS485_over_uart_init_interrupt()
{
	 NRF_UART0->INTENSET = UART_INTENSET_RXDRDY_Enabled<<UART_INTENSET_RXDRDY_Pos;
 	 NVIC_SetPriority(UART0_IRQn, 3U);
   NVIC_EnableIRQ(UART0_IRQn);
	 NRF_UART0->EVENTS_RXDRDY = 0;
}

void RS485_over_uart_putstring(const uint8_t * str)
{
    uint_fast8_t i  = 0;
    uint8_t      ch = str[i++];

		NRF_GPIO->OUT |= (1<<TransmitOrReceiveSignal_pin); // mode actif/transmission : sortie TransmitOrReceive = 1
    while (ch != '\0')
    {
        simple_uart_put(ch);
        ch = str[i++];
    }
		NRF_GPIO->OUT &= ~(1<<TransmitOrReceiveSignal_pin); // mode passif/�coute : sortie TransmitOrReceive = 0
}

void halUARTRegisterCb(t_uartInputDataCb *p_cbFunc_p)
{
   uartCallbackFunc_s = p_cbFunc_p;
}

/*---------------------------------------------------------------------------------------------+
|  Interrupts                                                                                  |
+---------------------------------------------------------------------------------------------*/
/**
 * UART IRQ handler
 */
void UART0_IRQHandler(void)
{
   if(uartCallbackFunc_s != NULL)
   {
      uartCallbackFunc_s();
   }
   /* clear events */
   NRF_UART0->EVENTS_RXDRDY = 0;
}
