/**
 * @file    hal_rtc.c
 * @author  Jan Grossrieder
 * @date    November, 2014
 * @brief   see hal_rtc.h
 */

/* headers for this module -------------------------------------------------------------------*/
#include "hal_rtc.h"
#include "nrf.h"

/* standard headers (ANSI- and Posix standards) ----------------------------------------------*/
#include <stdint.h>
#include <stdbool.h>
#include <stdio.h>

/*---------------------------------------------------------------------------------------------+
|  module global variables                                                                     |
+---------------------------------------------------------------------------------------------*/
/**
 * The registered callback function RTC1
 */
static t_rtcCompCb *rtcCallbackFunc_s = NULL;

/**
 * The registered callback function RTC0
 */
static t_rtcCompCb *rtc1CallbackFunc_s = NULL;

/*---------------------------------------------------------------------------------------------+
|  functions                                                                                   |
+---------------------------------------------------------------------------------------------*/
void halRTCInit(const s_rtcConfig_t *const p_config_p, bool START)
{
   /* power on RTC peripherie */
   NRF_RTC0->POWER = (RTC_POWER_POWER_Enabled << RTC_POWER_POWER_Pos);
   
   NRF_RTC0->PRESCALER = p_config_p->ui_prescaler;
   
   if(p_config_p->isCompareIntEnabled == true)
   {
      /* enable interrupt with middle priority */
      NVIC_SetPriority(RTC0_IRQn, 2U);
      NVIC_EnableIRQ(RTC0_IRQn);
      
      /* set compare value */
      NRF_RTC0->CC[0U] = p_config_p->ul_compareVal;
      
      /* enable interrupt on compare event */
      NRF_RTC0->INTENSET = 
         (RTC_INTENSET_COMPARE0_Enabled << RTC_INTENSET_COMPARE0_Pos);
      NRF_RTC0->EVTENSET = (RTC_EVTENSET_COMPARE0_Enabled << RTC_EVTENSET_COMPARE0_Pos);
      
      NRF_RTC0->EVENTS_COMPARE[0U] = 0UL;
   }
   
   NRF_RTC0->TASKS_START = 1UL;
}

void halRTCRegisterCb(t_rtcCompCb *p_cbFunc_p)
{
   rtcCallbackFunc_s = p_cbFunc_p;
}

void halRTC1Init(const s_rtcConfig_t *const p_config_p, bool START)
{
   /* power on RTC peripherie */
   NRF_RTC1->POWER = (RTC_POWER_POWER_Enabled << RTC_POWER_POWER_Pos);
   
   NRF_RTC1->PRESCALER = p_config_p->ui_prescaler;
   
   if(p_config_p->isCompareIntEnabled == true)
   {
      /* enable interrupt with middle priority */
      NVIC_SetPriority(RTC0_IRQn, 3U);
      NVIC_EnableIRQ(RTC1_IRQn);
      
      /* set compare value */
      NRF_RTC1->CC[0U] = p_config_p->ul_compareVal;
      
      /* enable interrupt on compare event */
      NRF_RTC1->INTENSET = 
         (RTC_INTENSET_COMPARE0_Enabled << RTC_INTENSET_COMPARE0_Pos);
      NRF_RTC1->EVTENSET = (RTC_EVTENSET_COMPARE0_Enabled << RTC_EVTENSET_COMPARE0_Pos);
      
      NRF_RTC1->EVENTS_COMPARE[0U] = 0UL;
   }
   
   NRF_RTC1->TASKS_START = START;
}

void halRTC1RegisterCb(t_rtcCompCb *p_cbFunc_p)
{
   rtc1CallbackFunc_s = p_cbFunc_p;
}

void halRTC1Start()
{
	NRF_RTC1->TASKS_START = 1;
}

void halRTC1Stop()
{
	NRF_RTC1->TASKS_STOP = 1;
}

void halRTC1Reset()
{
	NRF_RTC1->TASKS_CLEAR = 1UL;
}

void halRTC1ChangeCompVal(int CompVal)
{
	NRF_RTC1->TASKS_STOP = 1;
	NRF_RTC1->TASKS_CLEAR = 1UL;
	NRF_RTC1->CC[0U] = CompVal;
	NRF_RTC1->TASKS_START = 1;
}

/*---------------------------------------------------------------------------------------------+
|  Interrupts                                                                                  |
+---------------------------------------------------------------------------------------------*/
/**
 * RTC0 IRQ handler
 */
void RTC0_IRQHandler(void)
{
   if(rtcCallbackFunc_s != NULL)
   {
      rtcCallbackFunc_s();
   }
   
   /* clear counter */
   NRF_RTC0->TASKS_CLEAR = 1UL;
   /* clear events */
   NRF_RTC0->EVENTS_COMPARE[0U] = 0UL;
}

/**
 * RTC1 IRQ handler
 */
void RTC1_IRQHandler(void)
{
	   if(rtc1CallbackFunc_s != NULL)
   {
      rtc1CallbackFunc_s();
   }
   
   /* clear counter */
   NRF_RTC1->TASKS_CLEAR = 1UL;
   /* clear events */
   NRF_RTC1->EVENTS_COMPARE[0U] = 0UL;
}
