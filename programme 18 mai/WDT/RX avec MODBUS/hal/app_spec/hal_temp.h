/**
 * @file    hal_temp.h
 * @author  Jan Grossrieder
 * @date    November, 2014
 * @brief   Read out the silicon temperature of the chip
 */

#ifndef HAL_TEMP_H_INCLUDED
#define HAL_TEMP_H_INCLUDED

/* includes that must be known for this header -----------------------------------------------*/
#include <stdint.h>

/*---------------------------------------------------------------------------------------------+
|  function prototypes                                                                         |
+---------------------------------------------------------------------------------------------*/
/**
 * Gets the silicium temperature
 *
 * @return the temperature in 0.25 degrees Celsius
 */
int16_t halTempGetTemp(void);

#endif
